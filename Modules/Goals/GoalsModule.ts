module XXX.Tracker.System.Modules.Goals {
	import EventSystem = XXX.Package.EventSystem
	import Badges = XXX.Tracker.System.Badges;

	export class GoalsModule implements Modules.ModuleInterface {
		id: string = 'tracker.module.goals';
		private goals: XXX.Modules.Goals.GoalInterface[] = [];

		init() {
			EventSystem.Pipeline.send(new XXX.Modules.Goals.GoalPageLoad());

			EventSystem.Pipeline.bind(new EventSystem.Binding('goal.add', this.receiveAddGoalEvent.bind(this)));

			EventSystem.Pipeline.bind(new EventSystem.Binding('goal.target.update', this.receiveTargetUpdateEvent.bind(this)));

			EventSystem.Pipeline.bind(new EventSystem.Binding('goal.target.add', this.receiveTargetUpdateEvent.bind(this)));

			EventSystem.Pipeline.bind(new EventSystem.Binding('goal.target.delete', this.receiveTargetDeleteEvent.bind(this)));
		}

		private receiveAddGoalEvent(event: XXX.Modules.Goals.GoalAddEvent) {
			var badges = Badges.Badges.badges,
				badgesIds = [];

			for (var i = 0; i < badges.length; i++) {
				if (badges[i].id != 'editor.highlight') {
					badgesIds.push(badges[i].id)
				}
			}

			for (var i = 0; i < badgesIds.length; i++) {
				Badges.Badges.removeBadge(badgesIds[i]);
			}

			var goal: XXX.Modules.Goals.GoalInterface = event.data;

			goal.targets.forEach(function(target: XXX.Modules.Goals.TargetInterface) {
				if (target.type === 'landing') {
					return;
				}
				Badges.Badges.removeBadge(target._id);

				/*Check if target's url matches current page*/
				var rexp = new RegExp('^' + target.uri + '$', '');
				if (rexp.test(window.location.pathname)) {
					Badges.Badges.addBadge(new TargetBadge(goal, target));
				}

			});

			this.goals.push(goal);
		}

		private receiveTargetUpdateEvent(event: XXX.Modules.Goals.TargetUpdateEvent) {
			if (event.target.type === 'landing') {
				return;
			}
			Badges.Badges.removeBadge(event.target._id);
			Badges.Badges.addBadge(new TargetBadge(event.goal, event.target));
		}

		private receiveTargetDeleteEvent(event: XXX.Modules.Goals.TargetDeleteEvent) {
			Badges.Badges.removeBadge(event.target._id);
		}
	}
}
