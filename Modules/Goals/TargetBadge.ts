module XXX.Tracker.System.Modules.Goals {

	export class TargetBadge extends Badges.ElementBadge {

		constructor(goal: XXX.Modules.Goals.GoalInterface, target: XXX.Modules.Goals.TargetInterface) {
			super(target._id, target.path);

			this.target = <HTMLDivElement>document.querySelector(this.path);

			this.element.classList.add('__XXX_goal');
			this.element.dataset['XXXTargetName'] = target.name;

			if (this.target !== null) {
				this.target.dataset['XXXTargetId'] = target._id;
				this.target.dataset['XXXGoalId'] = goal._id;
			}

		}

		draw() {
			setInterval(() => {
				this.target = <HTMLDivElement>document.querySelector(this.path);
				super.draw();
			}, 500);
		}

		setName(name: string) {
			this.element.dataset['XXXTargetName'] = name;
		}

		remove() {
			if (this.target !== null) {
				this.target.removeAttribute('data-XXX-target-id');
				this.target.removeAttribute('data-XXX-goal-id');
			}

			super.remove();
		}
	}
}

