module XXX.Tracker.System.Modules.Variations {

	export class ModificationBadge extends Badges.ElementBadge {
		hidden: boolean = false;

		constructor(modification: XXX.Modules.Variations.ModificationInterface) {
			super(modification.id, modification.path);

			this.target = <HTMLDivElement>document.querySelector(this.path);
			this.type = modification.type;

			this.element.classList.add('__XXX_variation_modification');

			this.element.classList.add('__XXX_variation_modification_' + modification.type);
			this.element.setAttribute('data-selector', this.path);
		}

		draw() {
			setInterval(() => {
				if (!this.hidden) {
					this.target = <HTMLDivElement>document.querySelector(this.path);
					super.draw();
					this.show();
				} else {
					this.hide();
				}
			}, 500);
		}
	}
}
