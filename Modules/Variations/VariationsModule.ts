module XXX.Tracker.System.Modules.Variations {
	import Modules = XXX.Tracker.System.Modules;
	import EventSystem = XXX.Package.EventSystem;
	import DomPath = XXX.Setup.DomPath;

	export class VariationsModule implements Modules.ModuleInterface {
		id: string = 'tracker.module.variations';
		badges: ModificationBadge[] = [];
		hidden: boolean = false;
		variation: XXX.Modules.Variations.Variation;
		variations: Object = {};
		observeDOM = {
			spy: (function() {
				var MutationObserver = (<any>window).MutationObserver || (<any>window).WebKitMutationObserver,
					eventListenerSupported = window.addEventListener;

				return function(obj, callback) {
					if (MutationObserver) {
						var obs = new MutationObserver(function(mutations, observer) {
							if (mutations[0].addedNodes.length || mutations[0].removedNodes.length)
								callback();
						});

						obs.observe(obj, {childList: true, subtree: true});
					}
					else if (eventListenerSupported) {
						obj.addEventListener('DOMNodeInserted', callback, false);
						obj.addEventListener('DOMNodeRemoved', callback, false);
					}
				};
			})(),
			spyEnabled: true
		};

		public init() {
			EventSystem.Pipeline.bind(
				new EventSystem.Binding('variation.apply', this.receiveVariationApplyEvent.bind(this)
				));

			EventSystem.Pipeline.bind(
				new EventSystem.Binding('variation.modification.add', this.receiveVariationModificationAddEvent.bind(this)
				));

			EventSystem.Pipeline.bind(
				new EventSystem.Binding('variation.modification.highlight', this.applyHighlight.bind(this)
				));

			EventSystem.Pipeline.bind(
				new EventSystem.Binding('variation.show.deleted.elements', this.showDeletedElements.bind(this)
				));

			EventSystem.Pipeline.bind(
				new EventSystem.Binding('modification.delete', this.ModificationDeleted.bind(this)
				));

			EventSystem.Pipeline.bind(
				new EventSystem.Binding('draw.badge.on.move', this.drawBadgeOnMove.bind(this)
				));
		}

		private drawBadgeOnMove(event) {
			var modification = this.initModification(event.modification);
			this.addBadge(modification);
			this.removeBadge(modification);
			this.highlightCustomElements();
		}

		private ModificationDeleted(event: XXX.Modules.Variations.ModificationDeletedEvent) {
			var modification = this.initModification(event.modification);
			this.badges.forEach((badge, index) => {
				if (badge.type == 'remove' && badge.path == modification.path)
					delete this.badges[index];
			});
			var tmp_arr = [], cnt = 0;
			for (var key in this.badges) {
				tmp_arr[cnt] = this.badges[key];
				cnt++;
			}
			this.badges = tmp_arr;

		}

		private showDeletedElements(event: XXX.Modules.Variations.ShowDeletedElementsEvent) {
			this.badges.forEach((badge) => {
				if (badge.type != 'remove') return;

				var element = document.querySelector(badge.path);

				if (element) {
					if (!event.show) {
						badge.hidden = true;
						element.classList.add('__XXX_hidden');
					} else {
						badge.hidden = false;
						element.classList.remove('__XXX_hidden');
					}
				}
			});
		}

		private applyHighlight(event: XXX.Modules.Variations.ModificationHighlightEvent) {
			for (var i = 0; i < this.badges.length; i++) {
				if (event.highlight) {
					this.hidden = false;
					this.badges[i].hidden = false;
				} else {
					this.hidden = true;
					this.badges[i].hidden = true;
				}
			}
		}

		private receiveVariationModificationAddEvent(event: XXX.Modules.Variations.VariationModificationAddEvent) {
			console.log("Modification Received!");

			var variation = new XXX.Modules.Variations.Variation(event.variation.id);
			var modification = this.initModification(event.modification);


			//check if this modification already exists
			//******we don't need anymore?? was needed when we had apply event only****

			//if (this.addModification(variation.id, modification)){

			this.addBadge(modification);
			this.removeBadge(modification);
			variation.addModification(modification);

			this.variations[event.variation.id] = variation;

			variation.apply();

			//highlight new Elements that didn't exists in the document
			this.highlightCustomElements();
			//}

			// Resizing iframe after adding some changes
			EventSystem.Pipeline.send(new Setup.ViewerSetupEvent());
			EventSystem.EventSystem.broadcast(new Setup.ViewerSetupEvent());
		}

		private receiveVariationApplyEvent(event: XXX.Modules.Variations.VariationApplyEvent) {
			console.log("Variation Received!");
			if (typeof event.variation == 'undefined') return;

			var variation = new XXX.Modules.Variations.Variation(event.variation.id);
			var count: number = 0;

			event.variation.modifications.reverse();
			var index = event.variation.modifications.length - 1;

			while (index >= 0) {

				var modification = this.initModification(event.variation.modifications[index]);
				this.addBadge(modification);

				variation.addModification(modification);
				event.variation.modifications.splice(index, 1);
				index--;
			}

			variation.apply();
			this.variation = variation;
			localStorage.setItem('spyEnabled', 'true');
			this.observeDOM.spy(document.body, function() {
				if (this.observeDOM.spyEnabled && localStorage.getItem('spyEnabled')) {
					this.variation.apply();
					localStorage.setItem('spyEnabled', '');
				} else {
					localStorage.setItem('spyEnabled', 'true');
				}
			}.bind(this));

			//highlight new Elements that didn't exists in the document
			this.highlightCustomElements();

			// Resizing iframe after adding some changes
			EventSystem.Pipeline.send(new Setup.ViewerSetupEvent());
			EventSystem.EventSystem.broadcast(new Setup.ViewerSetupEvent());

		}

		private addBadge(modification: XXX.Modules.Variations.Modification) {

			var badge = new ModificationBadge(modification);
			var exludeTypes = ['new-text-box', 'new-html-box', 'disable-reposition', 'disable-resize'];
			if (!this.checkIfBadgeExists(badge) && !~exludeTypes.indexOf(modification.type)) {
				this.badges.push(badge);
				badge.hidden = this.hidden || modification.hiddenBadge;
				badge.draw();
			} else {
				badge.remove()
			}
			return modification;
		}

		private removeBadge(modification: XXX.Modules.Variations.Modification) {
			this.badges.forEach((badge, index) => {
				if ((modification.change == "1" || badge.type == modification.oppositeType) && badge.path == modification.path) {
					badge.remove();
					delete(this.badges[index]);
				}
			});
		}


		private highlightCustomElements() {

			var XXXElements = document.querySelectorAll('[data-drawBadge]');

			for (var i = 0; i < XXXElements.length; i++) {

				var current = <HTMLElement>XXXElements[i];
				var modification = new XXX.Modules.Variations.Modification(this.badges.length + 1 + '');
				modification.path = DomPath.getDomPathString(current);
				modification.type = 'highlight-custom';
				modification.change = '';

				var badge = new ModificationBadge(modification);
				this.badges.push(badge);
				badge.hidden = this.hidden;
				badge.draw();

				current.removeAttribute('data-drawBadge');
			}

		}

		private initModification(eventModification: XXX.Modules.Variations.Modification): XXX.Modules.Variations.Modification {

			var modification = new XXX.Modules.Variations.Modification(eventModification.id);
			modification.path = eventModification.path;
			modification.type = eventModification.type;
			modification.oppositeType = eventModification.oppositeType;
			modification.change = eventModification.change;
			modification.hiddenBadge = eventModification.hiddenBadge;
			modification.positionX = eventModification.positionX;
			modification.positionY = eventModification.positionY;

			modification.cohesion = Boolean(eventModification.cohesion);

			return modification;
		}

		private addModification(variationId: string, modification: XXX.Modules.Variations.ModificationInterface) {

			if (!this.variations[variationId]) return true;

			var variation = this.variations[variationId];
			if (variation.modifications.some((currentModification) => {
					if (currentModification.path == modification.path
						&& currentModification.change == modification.change
						&& currentModification.type == modification.type)
						return true;
				})) {
				return false;
			} else {
				return true;

			}

		}

		private checkIfBadgeExists(badge: ModificationBadge) {
			if (~badge.element.className.indexOf('remove')) return false;
			for (var i in this.badges) {
				if (this.badges[i].path == badge.path) return true;
			}

			return false;
		}

	}
}
