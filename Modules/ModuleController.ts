module XXX.Tracker.System.Modules {
	import EventSystem = XXX.Package.EventSystem

	export class ModuleController {

		public static modules: ModuleInterface[] = [];

		public static init() {
			this.installCss();
			this.setupViewer();
			EventSystem.Pipeline.init(parent);
		}

		public static importModule(mod: ModuleInterface) {
			this.modules.push(mod);
			mod.init();
		}

		public static get(id: string) {
			for (var i = 0; i < this.modules.length; i++) {
				if (this.modules[i].id == id) return this.modules[i];
			}
		}


		private static installCss() {
			var stylesheet = <HTMLLinkElement>document.createElement('link');
			stylesheet.rel = "stylesheet";
			stylesheet.type = "text/css";
			stylesheet.href = "//lab.XXX.com/tracker/style.css";

			document.head.appendChild(stylesheet);
		}

		private static setupViewer() {
			var checkSize = setInterval(function () {
				if (window.innerHeight > 0) {
					EventSystem.Pipeline.send(new Setup.ViewerSetupEvent());
					EventSystem.EventSystem.broadcast(new Setup.ViewerSetupEvent());
					clearInterval(checkSize);
				}
			}, 10);
		}
	}

}
