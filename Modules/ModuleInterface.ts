module XXX.Tracker.System.Modules {
	export interface ModuleInterface {
		id: string;

		init(): void;
	}
}
