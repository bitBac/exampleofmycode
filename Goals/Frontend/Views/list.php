<form method="post">
	<main>
		<?php if($goals_count) { ?>
			<header>
				<h1 class="page-title">
					Goals <?= \XXX\Gui\Base\Template\Common::docsLink($link = 'http://docs.XXX.com/view/goals/overview.php') ?></h1>
				<div class="filtering-options">
					<a class="button action" href="/<?= XXX\Common\Request::getWebsiteId(); ?>/goals/manage/"><i
								class="fa-plus"></i> Create Goal</a>
					<?= \XXX\Modules\General\FrontendAjax\Controller::filterLoad() ?>
				</div>
			</header>
			<ul class="page-tabs tabs">
				<li class="active" data-tab="list">
					<a href="#">
						List
					</a>
				</li>
				<li data-tab="analytics">
					<a href="#">
						Analytics
					</a>
				</li>
			</ul>
			<div class="panel" data-tab-group="list">
				<div class="header">
					<h2>Goals List</h2>
					<?= \XXX\Gui\Base\Template\Common::dataTableNav() ?>
				</div>
				<div class="content nopadding">
					<?= \XXX\Gui\Base\Template\Common::ajaxLoad(
						'/ajax/goals/goals/list/',
						'goals',
						array(
							'limit'            => 20,
							'date-range'       => 'date-range',
							'pagination-total' => true
						),
						true
					) ?>
				</div>
				<div class="footer">
					<div class="form-group">
						<select class="fauto" name="goals-action">
							<option value="">
								Select Option
							</option>
							<option value="deactivate">
								Deactivate Selected
							</option>
							<option value="activate">
								Activate Selected
							</option>
							<option value="delete"
							        data-confirm="Are you sure that you want to delete the selected accounts?">
								Delete Selected
							</option>
						</select>
						<button name="goals-submit" type="submit" class="button action"><i class="fa-check"></i> Save
						</button>
					</div>
				</div>
			</div>
			<div data-tab-group="analytics">
				<div class="content nopadding">
					<div class="row">
						<div class="col-6">
							<?= \XXX\Gui\Base\Template\Common::ajaxLoad(
								'/ajax/goals/results/load/types/',
								'types',
								array(
									'date-range'    => 'date-range',
									'filter-object' => "main"
								),
								true
							) ?>
						</div>
						<div class="col-6">
							<?= \XXX\Gui\Base\Template\Common::ajaxLoad(
								'/ajax/goals/results/load/goals-converted/',
								'converted',
								array(
									'date-range'    => 'date-range',
									'filter-object' => "main"
								),
								true
							) ?>
						</div>
					</div>
					<div class="panel">
						<div class="header">
							<h2>Top Goals</h2>
						</div>
						<?= \XXX\Gui\Base\Template\Common::ajaxLoad(
							'/ajax/goals/results/load/top-goals/',
							'goals',
							array(
								'date-range'    => 'date-range',
								'filter-object' => "main"
							),
							true
						) ?>
						<div class="footer">
							<a href="/<?= XXX\Common\Request::getWebsiteId(); ?>/goals/overview/"
							   class="view-more">
								See all Goals
							</a>
						</div>
					</div>
				</div>
			</div>
		<?php } else { ?>
			<?= \XXX\Gui\Base\Template\Common::noDataNotice(
				$title = "You haven't added any goals to your site!",
				$message = "",
				$button = "Create a Goal",
				$url = "/" . XXX\Common\Request::getWebsiteId() . "/goals/manage/"
			) ?>
		<?php } ?>
	</main>
</form>
