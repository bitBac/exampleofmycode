<link rel="stylesheet" href="/templates/portal/css/viewer.css"/>
<div data-XXX-app-import="portal/goals/controller"></div>
<form method="post" id="viewer-form">
	<main>
		<?php if(!empty($goal)) { ?>
			<header>
				<h1 class="page-title"
				    style="overflow:hidden; white-space:nowrap; text-overflow:ellipsis; width:45%;"><?= $goal['name'] ?> <?= \XXX\Gui\Base\Template\Common::docsLink($link = 'http://docs.XXX.com/view/goals/overview.php') ?></h1>
				<div class="form-group">
					<?= \XXX\Modules\General\FrontendAjax\Controller::filterLoad() ?>
					<button form="viewer-form" name="goal-submit" data-ignore-confirm="" data-unsaved-button=""
					        class="button" type="submit">
						<i class="fa-check"></i> Save Changes
					</button>
				</div>
			</header>
			<ul class="page-tabs tabs">
				<li class="active" data-tab="viewer">
					<a href="#viewer">
						View & Edit
					</a>
				</li>
				<li data-tab="basic">
					<a href="#basic">
						Overview
					</a>
				</li>
				<li data-tab="goal-performance">
					<a href="#goal-performance">
						Results
					</a>
					<ul class="sub-tabs subtabs">
						<li data-tab="goal-performance"><a href="#goal-performance">Goal Performance</a></li>
						<li data-tab="analytics"><a href="#analytics">Traffic Analytics</a></li>
						<li data-tab="recorded-sessions"><a href="#recorded-sessions">Session Explorer</a></li>
					</ul>
				</li>

			</ul>
		<?php } else { ?>
			<header>
				<h1 class="page-title">Create Goal</h1>
			</header>
		<?php } ?>
		<div <?php if(!empty($goal)) { ?>data-tab-group="basic"<?php } ?>>
			<div class="row">
				<div class="col-6">
					<div class="panel">
						<div class="header">
							<h2>Basic Information</h2>
						</div>
						<div class="content">
							<p class="form-group">
								<label class="fill">
									<label for="name">Goal Name</label>
									<input type="text" id="name" name="name" value="<?= $goal['name'] ?>"
									       placeholder="Goal Name" <?= (!empty($goal) ? "disabled" : "required") ?> />
								</label>
							</p>
							<p class="form-group">
								<label class="fill">
									<label for="scope">Multi-Step Settings</label>
									<select name="behaviour" id="behaviour" <?= (!empty($goal)) ? 'disabled' : '' ?>>
										<option value="all" <?= ($goal['behaviour'] == 'all') ? 'selected' : '' ?>>All
											steps are required, any order
										</option>
										<option value="custom" <?= ($goal['behaviour'] == 'custom') ? 'selected' : '' ?>>
											Some steps required, any order
										</option>
										<option value="funnel" <?= ($goal['behaviour'] == 'funnel') ? 'selected' : '' ?>>
											Advanced conversion funnel
										</option>
									</select>
								</label>
							</p>
							<p class="form-group" id="select-scope">
								<label class="fill">
									<label for="scope">Goal Rules</label>
									<select name="scope" id="scope" <?= (!empty($goal)) ? 'disabled' : '' ?>>
										<option value="site" <?= ($goal['scope']['type'] == 'site') ? 'selected' : '' ?>>
											Valid on any page on website
										</option>
										<option value="page" <?= ($goal['scope']['type'] == 'page') ? 'selected' : '' ?>>
											Valid only on a specific page
										</option>
										<option value="experiment" <?= ($goal['scope']['type'] == 'experiment') ? 'selected' : '' ?>>
											Valid in specific experiment
										</option>
									</select>
								</label>
							</p>

							<p class="additional-data form-group experiment <?= ($goal['scope']['type'] != 'experiment') ? 'hidden' : '' ?>">
								<label class="fill">
									<label for="experiment">Experiment</label>
									<label class="form-group">
										<select name="experiment" id="experiment"
										        class="select-extended" <?= (!empty($goal)) ? 'disabled' : '' ?>>
											<?php foreach($experiments as $experiment) { ?>
												<option
														value="<?= $experiment['_id'] ?>"
													<?= ($goal['scope']['experiment']['_id'] == $experiment['_id']) ? 'selected' : '' ?>
												><?= $experiment['name'] ?></option>
											<?php } ?>
										</select>
										<?php if(!empty($goal['scope']['experiment']['_id'])) { ?>
											<a class="button action"
											   href="/<?= XXX\Common\Request::getWebsiteId(); ?>/experiments/manage/<?= $goal['scope']['experiment']['_id'] ?>/">Go
												To Experiment</a>
										<?php } ?>
									</label>
								</label>
							</p>

							<p class="additional-data form-group page <?= ($goal['scope']['type'] != 'page') ? 'hidden' : '' ?>">
								<label class="fill">
									<label for="page">Page</label>
									<label class="form-group">
										<select name="page" id="page"
										        class="select-extended" <?= (!empty($goal)) ? '' : '' ?>>
											<?php foreach($pages as $page) { ?>
												<option value="<?= $page['_id'] ?>"
													<?= ($goal['scope']['page']['_id'] == $page['_id']) ? 'selected' : '' ?>
												><?= $page['uri'] ?><?= $page['subdomain'] ? "(" . $page['subdomain'] . ")" : '' ?></option>
											<?php } ?>
										</select>
										<?php if(!empty($goal['scope']['page']['_id'])) { ?>
											<a class="button action"
											   href="/<?= XXX\Common\Request::getWebsiteId(); ?>/pages/manage/<?= $goal['scope']['page']['_id'] ?>/#goals">Go
												To Page</a>
										<?php } ?>
									</label>
								</label>
							</p>

							<?php if(empty($goal)) { ?>
								<button class="button action" name="goal-submit"><i class="fa-plus"></i> Create Goal
								</button>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>


			<?php if(!empty($goal) && $goal['scope']['type'] != 'page') { ?>
				<div class="panel">
					<div class="header">
						<h2>Goals Exists on Pages</h2>
						<?= \XXX\Gui\Base\Template\Common::dataTableNav(5) ?>
					</div>
					<div class="content nopadding">
						<?= \XXX\Gui\Base\Template\Common::ajaxLoad(
							'/ajax/goals/pages/totals/list/',
							'conversions',
							array(
								'goal'          => (string) $goal['_id'],
								'date-range'    => 'date-range',
								'filter-object' => "main"
							),
							true) ?>
					</div>
				</div>
			<?php } ?>
			<?php if($goal['scope']['type'] == 'site') { ?>
				<div class="panel">
					<div class="header">
						<h2>Goal Exists on Experiments</h2>
						<?= \XXX\Gui\Base\Template\Common::dataTableNav(5) ?>
					</div>
					<div class="content nopadding">
						<?= \XXX\Gui\Base\Template\Common::ajaxLoad(
							'/ajax/goals/results/load/experiments/',
							'conversions',
							array(
								'goal'          => (string) $goal['_id'],
								'date-range'    => 'date-range',
								'filter-object' => "main"
							),
							true) ?>
					</div>
				</div>
			<?php } ?>
		</div>

		<?php if(!empty($goal)) { ?>
			<div data-tab-group="goal-targets">
				<div class="col-6">
					<div class="panel">
						<div class="header">
							<h2>Goal Targets</h2>
						</div>
						<div class="content nopadding">
							<div class='scroll-content'>
								<table class="basic-table">
									<thead>
									<tr>
										<th>Name</th>
										<th>Type</th>
										<th>URI</th>
									</tr>
									</thead>
									<tbody class="'scroll-content">
									<?php foreach($goal['targets'] as $target) { ?>
										<tr>
											<td><?= $target['name'] ?></td>
											<td><?= ucfirst($target['type']) ?></td>
											<td>
												<a href="/<?= XXX\Common\Request::getWebsiteId(); ?>/pages/manage/<?= $target['pageId'] ?>"><?= $target['uri'] ?></a>
											</td>
										</tr>
									<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>

			</div>

			<?php
			include_once(__DIR__ . '/goal-tabs/viewer.php');
			include_once(__DIR__ . '/results/analytics.php');
			include_once(__DIR__ . '/results/goal-performance.php');
			include_once(__DIR__ . '/results/recorded-sessions.php');
			?>

		<?php } ?>
	</main>
</form>
<script src="/templates/portal/js/Viewer/Viewer.min.js"></script>
<script src="/templates/portal/js/Goals/Goal.min.js"></script>