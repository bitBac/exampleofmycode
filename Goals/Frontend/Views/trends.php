<main>
	<header>
		<h1 class="page-title">Trends</h1>
		<?= \XXX\Modules\General\FrontendAjax\Controller::filterLoad() ?>
	</header>
	<div class="panel">
		<div class="header">
			<h2>Conversions</h2>
		</div>
		<?= \XXX\Gui\Base\Template\Common::ajaxLoad(
			'/ajax/goals/trends/load/conversions/',
			'conversions',
			array(
				'goal'          => (string) $goal['_id'],
				'date-range'    => 'date-range',
				'filter-object' => "main"
			),
			true
		) ?>
	</div>
</main>
