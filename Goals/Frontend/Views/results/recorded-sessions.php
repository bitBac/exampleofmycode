<div data-tab-group="recorded-sessions">


	<?php if(!empty($goal)) { ?>
		<div class="panel">
			<div class="header">
				<h2>Session Explorer with Goal Encountered</h2>
				<div class="table-text">
					<label class="table-options"
					       data-tooltip="Show sessions with less than one pageview or are less than 10 seconds long."
					       data-tooltip-attributes="wrap">
						Show Incomplete Sessions <input name="show-incomplete" type="checkbox"/>
					</label>
					<label class="table-options" data-tooltip="Show sessions with finished conversions."
					       data-tooltip-attributes="wrap">
						Show converted sessions <input name="show-conversion" type="checkbox"/>
					</label>
					<?= \XXX\Gui\Base\Template\Common::dataTableNav(5) ?>
				</div>
			</div>
			<div class="content nopadding">
				<?= \XXX\Gui\Base\Template\Common::ajaxLoad(
					'/ajax/goals/sessions/list/',
					'sessions',
					array(
						'goal'          => (string) $goal['_id'],
						'date-range'    => 'date-range',
						'filter-object' => "main"
					),
					true) ?>
			</div>
		</div>

	<?php } ?>


</div>
