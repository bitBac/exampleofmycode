<div data-tab-group="overview">
	<div class="panel">
		<div class="header">
			<h2>Goal Conversions</h2>
		</div>
		<div class="content nopadding">
			<?= \XXX\Gui\Base\Template\Common::ajaxLoad(
				'/ajax/goals/results/load/conversions/',
				'conversions',
				array(
					'goal'          => (string) $goal['_id'],
					'date-range'    => 'date-range',
					'filter-object' => "main"
				),
				true) ?>
		</div>
	</div>
	<div class="panel">
		<div class="header">
			<h2>Top Targets</h2>
			<a style="float: left; font-size: 2rem;" data-chart-toggle="top-targets">
				<i class="fa-bar-chart" data-type="column"></i>
				<i class="fa-area-chart hidden" data-type="line"></i>
			</a>
		</div>
		<div class="content nopadding static">
			<?= \XXX\Gui\Base\Template\Common::ajaxLoad(
				'/ajax/goals/results/load/target-graph/',
				'conversions',
				array(
					'goal'          => (string) $goal['_id'],
					'type'          => 'line',
					'date-range'    => 'date-range',
					'filter-object' => "main",
					'toggle'        => 'top-targets'
				),
				true) ?>
		</div>
		<?= \XXX\Gui\Base\Template\Common::ajaxLoad(
			'/ajax/goals/results/load/target-table/',
			'conversions',
			array(
				'goal'          => (string) $goal['_id'],
				'date-range'    => 'date-range',
				'filter-object' => "main",
			),
			true) ?>
	</div>
	<?php if($goal['scope']['type'] != 'page' && $goal['behaviour'] != 'funnel') { ?>
		<div class="panel">
			<div class="header">
				<h2>Pages Conversions</h2>
			</div>
			<div class="content nopadding">
				<?= \XXX\Gui\Base\Template\Common::ajaxLoad(
					'/ajax/goals/pages/totals/graph/',
					'conversions',
					array(
						'goal'          => (string) $goal['_id'],
						'date-range'    => 'date-range',
						'filter-object' => "main"
					),
					true) ?>
			</div>
		</div>
	<?php } ?>
	<?php if($goal['scope']['type'] == 'site') { ?>
		<div class="panel">
			<div class="header">
				<h2>Experiments Conversions</h2>
			</div>
			<div class="content nopadding">
				<?= \XXX\Gui\Base\Template\Common::ajaxLoad(
					'/ajax/goals/results/load/experiments-graph/',
					'conversions',
					array(
						'goal'          => (string) $goal['_id'],
						'date-range'    => 'date-range',
						'filter-object' => "main"
					),
					true) ?>
			</div>
		</div>
	<?php } ?>
</div>