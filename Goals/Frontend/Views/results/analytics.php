<div data-tab-group="analytics">
	<div class="row">
		<div class="col-4">
			<div class="panel">
				<div class="header">
					<h2>Operating Systems</h2>
				</div>
				<div class="content nopadding auto-resize">
					<?= \XXX\Gui\Base\Template\Common::ajaxLoad(
						'/ajax/goals/analytics/load/technology/oss/',
						'conversions',
						array(
							'goal'          => (string) $goal['_id'],
							'date-range'    => 'date-range',
							'filter-object' => "main"
						),
						true) ?>
				</div>
				<div class="footer">
					<a href="/<?= XXX\Common\Request::getWebsiteId(); ?>/analytics/technologies/?fk[goal]=<?= $goal['_id'] ?>"
					   class="view-more">
						See all Technologies
					</a>
				</div>
			</div>
		</div>
		<div class="col-4">
			<div class="panel">
				<div class="header">
					<h2>Browser</h2>
				</div>
				<div class="content nopadding auto-resize">
					<?= \XXX\Gui\Base\Template\Common::ajaxLoad(
						'/ajax/goals/analytics/load/technology/browsers/',
						'conversions',
						array(
							'goal'          => (string) $goal['_id'],
							'date-range'    => 'date-range',
							'filter-object' => "main"
						),
						true) ?>
				</div>
				<div class="footer">
					<a href="/<?= XXX\Common\Request::getWebsiteId(); ?>/analytics/technologies/?fk[goal]=<?= $goal['_id'] ?>"
					   class="view-more">
						See all Technologies
					</a>
				</div>
			</div>
		</div>
		<div class="col-4">
			<div class="panel">
				<div class="header">
					<h2>Device Categories</h2>
				</div>
				<div class="content nopadding auto-resize">
					<?= \XXX\Gui\Base\Template\Common::ajaxLoad(
						'/ajax/goals/analytics/load/devices/types_pie/',
						'conversions',
						array(
							'goal'          => (string) $goal['_id'],
							'date-range'    => 'date-range',
							'filter-object' => "main"
						),
						true) ?>
				</div>
				<div class="footer">
					<a href="/<?= XXX\Common\Request::getWebsiteId(); ?>/analytics/devices/?fk[goal]=<?= $goal['_id'] ?>"
					   class="view-more">
						See all Devices
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-6">
			<div class="panel">
				<div class="header">
					<h2>Geography</h2>
				</div>
				<div class="content nopadding">
					<?= \XXX\Gui\Base\Template\Common::ajaxLoad(
						'/ajax/goals/analytics/load/geography/world/',
						'conversions',
						array(
							'goal'          => (string) $goal['_id'],
							'date-range'    => 'date-range',
							'filter-object' => "main"
						),
						true) ?>
				</div>
				<div class="footer">
					<a href="/<?= XXX\Common\Request::getWebsiteId(); ?>/analytics/geography/?fk[goal]=<?= $goal['_id'] ?>"
					   class="view-more">
						See all Geography
					</a>
				</div>
			</div>
		</div>
		<div class="col-6">
			<div class="panel">
				<div class="header">
					<h2>Top Referrers</h2>
				</div>
				<div class="content nopadding">
					<?= \XXX\Gui\Base\Template\Common::ajaxLoad(
						'/ajax/goals/analytics/load/acquisition/general_referrers/',
						'conversions',
						array(
							'goal'          => (string) $goal['_id'],
							'date-range'    => 'date-range',
							'filter-object' => "main"
						),
						true) ?>
				</div>
				<div class="footer">
					<a href="/<?= XXX\Common\Request::getWebsiteId(); ?>/analytics/acquisition/?fk[goal]=<?= $goal['_id'] ?>"
					   class="view-more">
						See all Acquisition
					</a>
				</div>
			</div>
		</div>
	</div>
</div>