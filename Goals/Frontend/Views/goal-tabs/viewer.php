<div data-tab-group="viewer">
	<div id="viewer-settings">
		<div data-viewer-setting="protocol" data-viewer-setting-value="<?= $website['protocol'] ?>"></div>
		<div data-viewer-setting="goal" data-viewer-setting-value="<?= $goal['_id'] ?>"></div>
		<div data-viewer-setting="module" data-viewer-setting-value="goals"></div>
		<div data-viewer-setting="domain" data-viewer-setting-value="<?= $website['url'] ?>"></div>
		<div data-viewer-setting="subdomain" data-viewer-setting-value="<?= $subdomain ?>"></div>
		<div data-viewer-setting="page" data-viewer-setting-value="<?= $goal['pageId'] ?>"></div>
		<div data-viewer-setting="uri-goal" data-viewer-setting-value="<?= $goal['uri'] ?>"></div>
		<div data-viewer-setting="active_goals_remaining"
		     data-viewer-setting-value="<?= $limits['active_goals_remaining']; ?>"></div>
	</div>
	<div class="viewer goal-viewer">
		<div class="viewer-controls sticky">
			<div class="settings">
				<ul class='targets'>

				</ul>
				<ul class="options">
					<li>
						<a data-user-settings="targets-list">Targets List</a>
					</li>
					<li>
						<a class="fa fullscreen-button" data-user-settings="viewer-fullscreen" data-enabled="false"></a>
					</li>
				</ul>
			</div>
			<div class="navigation form-group">

			</div>
		</div>

		<div class="panel goal-modals"></div>

		<div class="viewport-wrapper">
			<div class="panel-notification error"></div>
			<div class="viewer-alert"></div>
			<div class="viewport-overlay" onclick="" oncontextmenu="return false;"></div>
			<div class="viewport">
				<div class="viewer-legend scrollmap">
					<div class="breakpoint">Most Visible</div>
					<div class="breakpoint">Least Visible</div>
				</div>
				<iframe name="XXX_viewer" scrolling="auto" seamless="seamless" data-variations="true"
				        sandbox="allow-same-origin allow-scripts allow-forms allow-top-navigation" id="site-preview"
				        src="" style="height: 1000px;">

				</iframe>
			</div>
		</div>
	</div>
</div>
