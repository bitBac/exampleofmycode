<?php
/**
 * XXX
 * Copyright (C) XXX 2014
 */

namespace XXX\Modules\Goals\Frontend;

use XXX\Common;
use XXX\Modules\Goals;
use XXX\Modules\Experiments;
use XXX\Modules\Pages;
use XXX\Modules\Websites;
use Flint\Locale;

class Controller extends Common\Modules\AbstractEndController {

	public static function overview() {
		if(POST && self::posted('goals-submit')) {
			switch(self::post('goals-action')) {
				case 'activate':
					$affected = Goals\Model::updateGoals(
						(array) self::post('goals'),
						array('active' => true)
					);

					$message = "Activated {$affected['nModified']} goals.";
					break;

				case 'deactivate':
					$affected = Goals\Model::updateGoals(
						(array) self::post('goals'),
						array('active' => false)
					);

					$message = "Deactivated {$affected['nModified']} goals.";
					break;

				case 'delete':
					$affected = Goals\Model::updateGoals(
						(array) self::post('goals'),
						array('active' => false, 'deleted' => true)
					);

					$message = "Deleted {$affected['nModified']} goals.";
					break;
			}

			Goals\Model::notification('alert')
			           ->message($message)
			           ->type('success')
			           ->save();
		}

		$goals_count = Goals\Model::getGoals()
		                          ->website(self::currentWebsite()
		                                        ->getId())
		                          ->deleted(false)
		                          ->find()
		                          ->paginate();

		self::template()
		    ->assign('goals_count', $goals_count)
		    ->display('list');
	}

	public static function trends() {
		self::template()
		    ->display('trends');
	}

	public static function generateGoalUrl($goal) {
		$url = "/" . \XXX\Common\Request::getWebsiteId();
		$type = $goal['scope']['type'];
		if($goal['scope']['type'] == 'site') {
			$url .= "/pages/overview/";
		} else {
			if(isset($goal['scope'][$type])) {
				$_id = $goal['scope'][$type]['_id'];
				$url .= "/{$type}s/manage/{$_id}/#goals";
			} else {
				$url .= "/{$type}s/overview/";
			}
		}

		return $url;
	}

	public static function manage($_id) {
		if(POST && self::posted('goal-submit')) {
			if(empty($_id)) {
				if(self::currentWebsite()
				       ->allowedActiveGoal()
				) {
					$active = true;
				} else {
					$active = false;
				}

				$goal = Goals\Objects\Goal::create(
					self::post('name'),
					self::user()
					    ->getAccount()
					    ->getId(),
					self::currentWebsite()
					    ->getId(),
					Locale\Time::utc(),
					null,
					$active
				);

				$scope = self::post('scope');
				$objId = ($scope != 'site') ? self::post($scope) : null;

				$behaviour = self::post('behaviour');
				$goal->set('behaviour', $behaviour);

				$goal->set('type', self::post('type'));
				$page = Pages\Objects\Page::load(self::post('page'));
				if(!empty($page->get('subdomain'))) {
					$goal->set('subdomain', $page->get('subdomain'));
				}
				$goal->setScope($scope, $objId)
				     ->save();

				self::redirect("/" . \XXX\Common\Request::getWebsiteId() . "/goals/manage/" . $goal->get('_id') . "/");
			} else {
				if(self::posted('goals')) {
					$goal = Goals\Objects\Goal::load($_id);
					$obj = null;

					if($goal->get('scope.type') == 'page') {
						$obj = Pages\Objects\Page::load($goal->get('scope.page._id'));

						if(empty($obj->getId()) && $goal->get('scope.page.uri')) {
							$obj = Pages\Objects\Page::lookup($goal->get('website._id'), $goal->get('scope.page.uri'));
						}
					} else {
						if($goal->get('scope.type') == 'experiment') {
							$obj = Experiments\Objects\Experiment::load($goal->get('scope.experiment._id'));
						}
					}

					Goals\Model::processViewerGoals(
						self::post('goals'),
						$goal->get('website._id'),
						$obj
					);
				}
			}
		}

		$website = Websites\Objects\Website::load(Common\Request::getWebsiteId())
		                                   ->verifyAccess(self::account());

		$experiments = Experiments\Model::getExperiments(array('_id' => 1, 'name' => 1))
		                                ->website($website->getId())
		                                ->find();

		$pages = iterator_to_array(Pages\Model::getPages(array('_id' => 1, 'uri' => 1, 'subdomain' => 1))
		                                      ->website($website->getId())
		                                      ->find());

		foreach($pages as $key => $page) {
			if(isset($page['subdomain'])) {
				$sub = self::currentWebsite()
				           ->getSubdomainById($page['subdomain']);
				if($sub) {
					$pages[$key]['subdomain'] = $sub;
				}
			}
		}

		self::template()
		    ->assign('website.variables', $website->get('variables'))
		    ->assign('website', $website->get())
		    ->assign('experiments', $experiments)
		    ->assign('pages', $pages);

		if(!empty($_id)) {
			$goal = Goals\Objects\Goal::load($_id);

			$website = Websites\Objects\Website::load($goal->get('website._id'))
			                                   ->verifyAccess(self::account());

			if(self::currentWebsite()
			       ->getId() != $website->getId()
			) {
				self::redirect("/" . \XXX\Common\Request::getWebsiteId() . "/goals/overview/");
			}

			$_goal = $goal->get();

			foreach($_goal['targets'] as &$target) {
				$target['pageId'] = Pages\Objects\Page::lookup($website->getId(), $target['uri'])
				                                      ->getId();
			}

			if($_goal['scope']['type'] == 'experiment') {
				$experiment = Experiments\Objects\Experiment::load($_goal['scope']['experiment']['_id']);
				$_goal['uri'] = $experiment->get('uri');
			} else {
				if($_goal['scope']['type'] == 'page') {
					$page = Pages\Objects\Page::load($_goal['scope']['page']['_id']);
					$_goal['uri'] = $page->get('uri');
				} else {
					$_goal['uri'] = '/';
				}
			}
			$_goal['pageId'] = Pages\Objects\Page::lookup($_goal['website']['_id'], $_goal['uri'])
			                                     ->get('_id');
			!empty($_goal['subdomain']) ? $subdomain = $website->getSubdomainById($_goal['subdomain']) : $subdomain = '';
			self::template()
			    ->assign('goal', $_goal)
			    ->assign('subdomain', $subdomain);
		}

		self::template()
		    ->assign('allowed_active', self::currentWebsite()
		                                   ->allowedActiveGoal())
		    ->display('goal');
	}
}
