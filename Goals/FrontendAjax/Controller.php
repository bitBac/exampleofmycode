<?php
/**
 * XXX
 * Copyright (C) XXX 2014
 */

namespace XXX\Modules\Goals\FrontendAjax;

use XXX\Common;
use XXX\Modules\Goals;
use XXX\Modules\Experiments;
use XXX\Modules\Pages;
use XXX\Modules\Sessions;
use XXX\Modules\Websites\Objects\Website;
use Flint\Locale;

class Controller extends Common\Modules\AbstractAjaxController {

	public static function goalsList() {
		$goals = new Goals\Objects\AggregateReports\GoalsReport(self::currentWebsite());
		$goals->sort(
			Common\Objects\Sort::fallback('_id', -1)
		);
		$goals
			->filter(Common\Aggregates\Filtering\FilterGroup::auto())
			->dateRange(new Common\Objects\DateTimeRange());

		self::template()
		    ->assign('goals', $goals->getOverview(20))
		    ->display('overview/list');
	}

	public static function goalsTotal() {
		$goals = new Goals\Objects\AggregateReports\GoalsReport(self::currentWebsite());

		$items = $goals->getOverviewTotal();
		$items = (empty($items[0]['total']) ? 0 : $items[0]['total']);
		$pages = ceil($items / self::post('limit'));

		self::template()
		    ->assign('items', $items)
		    ->assign('pages', ($pages == 0) ? 1 : $pages)
		    ->displayGeneral('pagination/list-total');
	}

	public static function componentsLoad($modal) {
		$experiment = null;
		$page = null;

		if(!empty(self::post('experiment'))) {
			$experiment = \MongoDb\castIds(self::post('experiment'));
		}

		if(!empty(self::post('page'))) {
			$page = \MongoDb\castIds(self::post('page'));
		}

		self::template()
		    ->assign('websiteId', self::currentWebsite()
		                              ->getId());

		switch(strtolower($modal)) {
			case "add-goal":
				self::template()
				    ->assign('experiment', $experiment)
				    ->assign('page', $page)
				    ->display('viewer/modals/add-goal');
				break;
			case "add-goal-target":
				self::template()
				    ->assign('experiment', $experiment)
				    ->assign('page', $page)
				    ->display('viewer/modals/add-goal-target');
				break;
			case "edit-goal-target":
				self::template()
				    ->assign('experiment', $experiment)
				    ->assign('page', $page)
				    ->display('viewer/modals/edit-goal-target');
				break;
			case "edit-goal":
				self::template()
				    ->assign('experiment', $experiment)
				    ->assign('page', $page)
				    ->display('viewer/modals/edit-goal');
				break;
			case "delete-goal":
				self::template()
				    ->display('viewer/modals/delete-goal');
				break;
			case "goals-list":
				self::template()
				    ->display('viewer/modals/goals-list');
				break;
			case "targets-list":
				self::template()
				    ->display('viewer/modals/targets-list');
				break;
			case "new-goal":
				self::template()
				    ->display('viewer/new-goal');
				break;
		}
	}

	public static function trendsLoad($type) {
		switch($type) {
			case 'conversions':
				$goals = new Goals\Objects\AggregateReports\GoalsReport(self::currentWebsite());

				$daterange = new Common\Objects\DateTimeRange();

				$goals
					->dateRange($daterange);

				self::template()
				    ->assign('totals', $goals->renderConversionTotals())
				    ->assign('graph', $goals->renderConversionsGraph())
				    ->assign('meta.days', $daterange->getInterval()->days + 1)
				    ->display('trends/conversions');
		}
	}

	public static function resultsLoad($type) {
		switch($type) {
			case 'conversions':
				$goal = Goals\Objects\Goal::load(self::post('goal'));
				$report = new Goals\Objects\AggregateReports\GoalReport($goal);
				$range = new Common\Objects\DateTimeRange();
				$report
					->dateRange($range)
					->filter(Common\Aggregates\Filtering\FilterGroup::auto())
					->renderConversionSummary()
					->renderConversionGraph();

				self::template()
				    ->assign('report', $report->export()['renders'])
				    ->assign('meta.days', $range->getInterval()->days + 1)
				    ->display('results/conversions');
				break;
			case 'goals-converted':
				$report = new Goals\Objects\AggregateReports\GoalsReport(self::currentWebsite());
				$graph = $report->renderConvertedGoals();

				self::template()
				    ->assign('series', $graph)
				    ->display('analytics/goals/goal-converted');
				break;
			case 'types':
				$report = new Goals\Objects\AggregateReports\GoalsReport(self::currentWebsite());
				$graph = $report->renderGoalsTypes();

				self::template()
				    ->assign('series', $graph)
				    ->display('analytics/goals/types');
				break;
			case 'targets-funnel':
				$goal = Goals\Objects\Goal::load(self::post('goal'));

				$report = new Goals\Objects\AggregateReports\GoalReport($goal);

				$daterange = new Common\Objects\DateTimeRange();
				$report
					->dateRange($daterange)
					->filter(Common\Aggregates\Filtering\FilterGroup::auto())
					->renderFunnelGraph();

				self::template()
				    ->assign('series', $report->export()['renders']['graph'])
				    ->assign('totals', $report->export()['renders']['totals'])
				    ->assign('goal', $goal->get())
				    ->display('results/targets-funnel');

				break;
			case 'target-graph':
				$goal = Goals\Objects\Goal::load(self::post('goal'));

				$report = new Goals\Objects\AggregateReports\GoalReport($goal);

				$daterange = new Common\Objects\DateTimeRange();
				$report
					->dateRange($daterange)
					->filter(Common\Aggregates\Filtering\FilterGroup::auto());

				switch(self::post('type')) {
					case "line":
						$report->renderTargetConversionGraph();

						self::template()
						    ->assign('goal', $report->export())
						    ->display('results/target-graph');
						break;
					case "column":
						$report->renderTargetEventsColumn();

						self::template()
						    ->assign('goal', $report->export())
						    ->display('results/target-bar');
						break;
				}

				break;
			case 'target-table':
				$goal = Goals\Objects\Goal::load(self::post('goal'));

				$report = new Goals\Objects\AggregateReports\GoalReport($goal);

				$daterange = new Common\Objects\DateTimeRange();
				$report
					->dateRange($daterange)
					->filter(Common\Aggregates\Filtering\FilterGroup::auto())
					->renderConversionSummary()
					->renderTargetSummaries();

				self::template()
				    ->assign('meta.days', $daterange->getInterval()->days + 1)
				    ->assign('goal', $report->export()['renders'])
				    ->display('results/target-table');

				break;
			case 'top-goals':
				$goals = new Goals\Objects\AggregateReports\GoalsReport(self::currentWebsite());

				$goals->dateRange(new Common\Objects\DateTimeRange());

				$goals->sort(
					new Common\Objects\Sort('conversions', -1)
				);

				$rendered = array();
				foreach($goals->generateIds(5) as $goal_id) {
					try {
						$goal = Goals\Objects\Goal::load(
							$goal_id,
							array(
								'_id'         => 1,
								'website._id' => 1,
								'scope.type'  => 1,
								'name'        => 1
							)
						);

						$report = new Goals\Objects\AggregateReports\GoalReport($goal);

						$export = $report->dateRange(new Common\Objects\DateTimeRange())
						                 ->filter(Common\Aggregates\Filtering\FilterGroup::auto())
						                 ->renderConversionSummary()
						                 ->renderConversionGraph()
						                 ->export();

						if($export['renders']['conversions']['total'] > 0) {
							$rendered[] = $export;
						}

					} catch(\Exception $e) {

					}
				}

				self::template()
				    ->assign('goals', $rendered)
				    ->assign('goalsCount', $goals->getOverviewTotalCount())
				    ->display('results/top-goals');

				break;
			case 'experiments':
				$goal = Goals\Objects\Goal::load(self::post('goal'));
				$report = new Goals\Objects\AggregateReports\GoalReport($goal);
				$range = new Common\Objects\DateTimeRange();
				$report
					->dateRange($range)
					->filter(Common\Aggregates\Filtering\FilterGroup::auto())
					->sort(new Common\Objects\Sort('conversions', -1))
					->renderExperimentResults();

				self::template()
				    ->assign('experiments', $report->export()['renders']['experiments'])
				    ->display('results/experiments/applicable');
				break;
			case 'experiments-graph':
				$goal = Goals\Objects\Goal::load(self::post('goal'));
				$report = new Goals\Objects\AggregateReports\GoalReport($goal);
				$range = new Common\Objects\DateTimeRange();
				$report
					->dateRange($range)
					->filter(Common\Aggregates\Filtering\FilterGroup::auto())
					->sort(new Common\Objects\Sort('conversions', -1))
					->renderExperimentsGraph();

				self::template()
				    ->assign('series', $report->export()['renders'])
				    ->display('results/experiments/graph');
				break;
		}
	}

	public static function analyticsLoad($section, $name) {
		$goal = Goals\Objects\Goal::load(self::post('goal'));
		self::template()
		    ->assign('goal', $goal->get());

		$report = new Goals\Objects\AggregateReports\GoalReport($goal);

		$daterange = new Common\Objects\DateTimeRange();
		$report
			->dateRange($daterange)
			->filter(Common\Aggregates\Filtering\FilterGroup::auto());

		switch($section) {
			case 'technology':
				switch($name) {
					case 'browsers':
						$report->renderBrowserChart();
						$totals = $report->renderBrowserTotals();

						self::template()
						    ->assign('graph', $report->export()['renders']['charts']['browsers'])
						    ->assign('totals', $totals)
						    ->display('analytics/technology/browsers');
						break;
					case 'oss':
						$report->renderOssChart();
						$totals = $report->renderOssTotals();

						self::template()
						    ->assign('graph', $report->export()['renders']['charts']['os'])
						    ->assign('totals', $totals)
						    ->display('analytics/technology/oss');
						break;
				}
				break;
			case 'devices':
				switch($name) {
					case 'types_pie':
						$report->renderDevicesChart();
						$totals = $report->renderDeviceTotals();

						self::template()
						    ->assign('graph', $report->export()['renders']['charts']['devices'])
						    ->assign('totals', $totals)
						    ->display('analytics/devices/types_pie');
						break;
				}
				break;
			case 'geography':
				switch($name) {
					case 'world':
						$report->renderWorldMapConversions();

						self::template()
						    ->assign('goal', $report->export())
						    ->assign('codes', Locale\Geo::getCountries())
						    ->display('analytics/geography/worldmap');
						break;
				}
				break;
			case 'acquisition':
				switch($name) {
					case 'general_referrers':
						$report->renderReferrers();

						self::template()
						    ->assign('referrers', $report->export()['renders']['conversions']['referrers'])
						    ->display('analytics/acquisition/general_referrers');
						break;
				}
				break;
		}
	}

	public static function pagesTotals($type) {
		$goal = Goals\Objects\Goal::load(self::post('goal'));
		$range = new Common\Objects\DateTimeRange();
		$report = new Goals\Objects\AggregateReports\GoalReport($goal);
		$report
			->dateRange($range)
			->filter(Common\Aggregates\Filtering\FilterGroup::auto());

		switch($type) {
			case 'list':
				$report->renderPagesList();

				self::template()
				    ->assign('pages', $report->export()['renders'])
				    ->display('pages/list');
				break;
			case 'graph':
				$report->renderConversionPages();

				self::template()
				    ->assign('series', $report->export()['renders'])
				    ->display('pages/graph');

				break;
		}
	}

	public static function sessionsList() {
		$incomplete = false;
		$goal = null;
		$conversion = null;

		if(self::posted('goal') && !empty(self::post('goal'))) {
			$goal = (string) self::post('goal');
		}
		if(self::posted('showIncompleteSessions') && !empty(self::post('showIncompleteSessions'))) {
			$incomplete = false;
		}
		if(self::posted('showConversionSessions') && !empty(self::post('showConversionSessions'))) {
			$conversion = true;
		}

		$fg = Common\Aggregates\Filtering\FilterGroup::auto();

		if(!empty(self::post('filterIdentifier')) && !empty(self::post('filterValue'))) {
			$fg->register(new Common\Aggregates\Filtering\Filters\SessionsFilter(
				self::post('filterIdentifier'),
				self::post('filterValue'),
				null,
				self::post('filterExtra')
			));
		}

		$sessions = Sessions\Model::getSessionsAggregate()
		                          ->website(self::currentWebsite())
		                          ->dateTimeRange('created', new Common\Objects\DateTimeRange())
		                          ->goal(array($goal))
		                          ->parsed(true)
		                          ->incomplete($incomplete)
		                          ->length(5)
		                          ->filter($fg)
		                          ->sort(Common\Objects\Sort::fallback('created', -1))
		                          ->unwind('$summary.goals')
		                          ->match(array(
			                          'summary.goals._id' => \MongoDb\castIds($goal),
		                          ))
		                          ->withConversions($conversion)
		                          ->paginate(5)
		                          ->aggregate();

		self::template()
		    ->assign('sessions', $sessions)
		    ->display('sessions/list');
	}

}
