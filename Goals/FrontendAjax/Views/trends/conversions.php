<script>
	//require(['highcharts/highcharts', 'extensions/highcharts/XXX-theme'], function() {
	(function () {
		var charts = {};
		charts.overview = {
			renderConversions: function () {
				new Highcharts.Chart(Highcharts.merge(Highcharts.XXX.default.options, Highcharts.XXX.default.line, {
					chart: {
						type: 'area',
						renderTo: 'chart-conversions'
					},
					series: [{
						name: 'Conversions',
						data: <?=json_encode($graph['conversions'])?>
					}]
				}));
			}
		};
		$(function () {
			charts.overview.renderConversions();
		});
	})();
</script>
<div class="content nopadding">
	<div class="chart line" id="chart-conversions"></div>
	<div class="statistics">
		<div class="statistic">
			<div class="metric">
				<div class="title">
					Conversions
				</div>
				<strong class="value">
					<?= \XXX\Gui\Base\Template\Common::round($totals['conversions']['total']) ?>
				</strong>
			</div>
		</div>
		<div class="statistic">
			<div class="metric">
				<div class="title">
					New Visitor Conversions
				</div>
				<strong class="value">
					<?= \XXX\Gui\Base\Template\Common::round($totals['conversions']['sessions']['new']) ?>
				</strong>
			</div>
		</div>
		<div class="statistic">
			<div class="metric">
				<div class="title">
					Repeat Visitor Conversions
				</div>
				<strong class="value">
					<?= \XXX\Gui\Base\Template\Common::round($totals['conversions']['sessions']['returning']) ?>
				</strong>
			</div>
		</div>
		<div class="statistic">
			<div class="metric">
				<div class="title">
					Avg. Conversions/Day
				</div>
				<strong class="value">
					<?php if($meta['days'] > 0) { ?>
						<?= \XXX\Gui\Base\Template\Common::round($totals['conversions']['total'] / $meta['days']) ?>
					<?php } else { ?>
						0
					<?php } ?>
				</strong>
			</div>
		</div>
	</div>
