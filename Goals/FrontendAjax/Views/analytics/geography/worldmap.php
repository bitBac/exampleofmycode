<script>
	//require([
	//	'highcharts/highcharts',
	//	'extensions/highcharts/XXX-theme',
	//], function() {
	(function () {
		var charts = {};

		//require(['highmaps/modules/map', 'extensions/highmaps/world-highres'], function() {
		(function () {
			charts.overview = {
				renderWorldMap: function () {
					new Highcharts.Map(Highcharts.merge(Highcharts.XXX.map.options, {
						chart: {
							type: 'map',
							renderTo: 'map-world-map'
						},
						series: [{
							data: <?=json_encode($goal['renders']['charts']['worldmap'])?>,
							mapData: Highcharts.maps['custom/world-highres'],
							joinBy: 'hc-key',
							name: 'Conversions'
						}],
						joinBy: 'hc-key',
						name: 'Conversions',
						dataLabels: {
							enabled: false,
							format: '{point.name}'
						}
					}));
				}

			};
			$(function () {
				charts.overview.renderWorldMap();
			});
		})();

	}());
</script>
<div class="content nopadding">
	<div class="chart map large" id="map-world-map"></div>
</div>
<?php if(!empty($goal['renders']['conversions']['countries'])) { ?>
	<div class="content nopadding">
		<table class="data-table">
			<thead>
			<tr>
				<th class="t40">
					Country/Territory
				</th>
				<th class="icon">
					<i class="fa-users"></i>
				</th>
				<th>
					Views
				</th>
				<th>
					Clicks
				</th>
				<th>
					Conversions
				</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach($goal['renders']['conversions']['countries'] as $country) { ?>
				<tr>
					<td class="nowrap">
						<?= $codes[strtoupper($country['_id'])] ?>
					</td>
					<td class="icon">
						<a href="/<?= XXX\Common\Request::getWebsiteId(); ?>/explorer/overview/?fk[locale.location.country]=<?= urlencode($country['_id']) ?>&fk[goal]=<?= $goal['_id'] ?>"><i
									class="ci-recorded-sessions-dark"></i></a>
					</td>
					<td>
						<?= \XXX\Gui\Base\Template\Common::round($country['views']) ?>
					</td>
					<td>
						<?= \XXX\Gui\Base\Template\Common::round($country['clicks']) ?>
					</td>
					<td>
						<?= \XXX\Gui\Base\Template\Common::round($country['conversions']) ?>
					</td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>
<?php } else { ?>
	<div class="content">
		<span class="txt-info">
			<i class="fa-info-circle"></i> There is no country data for the dates and filters selected.
		</span>
	</div>
<?php } ?>
