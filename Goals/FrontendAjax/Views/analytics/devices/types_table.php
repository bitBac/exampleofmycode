<?php if(!empty($report['devices']['list']['totals']['types'])) { ?>
	<div class="content nopadding">
		<table class="data-table">
			<thead>
			<tr>
				<th>
					Device Category
				</th>
				<th>
					Sessions
				</th>
				<th>
					% New Visitors
				</th>
				<th>
					Bounce Rate
				</th>
				<th>
					Pages / Sessions
				</th>
				<th>
					Avg. Duration
				</th>
				<th>
					Goal Conversions
				</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach($report['devices']['list']['totals']['types'] as $type => $stats) { ?>
				<tr>
					<td>
						<?php if($type == 'smartphone') {
							$type = 'Phone';
						} ?>
						<?= ucfirst($type) ?>
					</td>
					<td>
						<?= \XXX\Gui\Base\Template\Common::round($stats['sessions']['total']) ?>
					</td>
					<td>
						<?= ($stats['sessions']['total'] > 0) ? number_format($stats['sessions']['new_visitors'] / $stats['sessions']['total'] * 100) . '%' : 'Pending' ?>
					</td>
					<td>
						<?= ($stats['sessions']['total'] > 0) ? number_format($stats['sessions']['bounced'] / $stats['sessions']['total'] * 100) . '%' : 'Pending' ?>
					</td>
					<td>
						<?= ($stats['sessions']['total'] > 0) ? number_format($stats['requests']['total'] / $stats['sessions']['total']) : 'Pending' ?>
					</td>
					<td>
						<?= ($stats['sessions']['total'] > 0) ? gmdate("H:i:s", $stats['sessions']['active_time'] / $stats['sessions']['total']) : 'Pending' ?>
					</td>
					<td>
						<?= \XXX\Gui\Base\Template\Common::round(111) ?>
					</td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>
<?php } else { ?>
	<div class="content">
		<span class="txt-info">
			<i class="fa-info-circle"></i> There is no device data for the dates and filters selected.
		</span>
	</div>
<?php } ?>
