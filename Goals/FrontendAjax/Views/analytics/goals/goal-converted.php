<div class="panel">
	<div class="header">
		<h2>Converted Goals</h2>
	</div>
	<div class="chart pie" id="chart-sessions-converted" data-highcharts-chart="1"></div>
	<script>
		(function () {
			var charts = {};
			charts.conversions = {
				renderConvertedSessions: function () {
					new Highcharts.Chart(Highcharts.merge(Highcharts.XXX.default.options, Highcharts.XXX.default.pie, {
						chart: {
							type: 'pie',
							renderTo: 'chart-sessions-converted'
						},
						series: [{
							name: 'Goals',
							data: <?= json_encode($series) ?>
						}]
					}));
				}
			};
			$(function () {
				charts.conversions.renderConvertedSessions();
			});
		})();
	</script>
</div>