<script>
	//require(['highcharts/highcharts', 'extensions/highcharts/XXX-theme'], function() {
	(function () {
		var charts = {};
		charts.overview = {
			renderOperatingSystem: function () {
				new Highcharts.Chart(Highcharts.merge(Highcharts.XXX.default.options, Highcharts.XXX.default.pie, {
					chart: {
						type: 'pie',
						renderTo: 'chart-operating-system'
					},
					series: [{
						name: 'Conversions',
						data: <?=json_encode($graph)?>
					}]
				}));
			}
		};
		$(function () {
			charts.overview.renderOperatingSystem();
		});
	})();
</script>
<div class="content nopadding">
	<div class="chart pie" id="chart-operating-system"></div>
</div>
<?php if(!empty($graph)) { ?>
	<div class="content nopadding">
		<table class="data-table">
			<thead>
			<tr>
				<th class="t25">
					OS
				</th>
				<th class="icon">
					<i class="fa-users"></i>
				</th>
				<th>
					Views
				</th>
				<th>
					Clicks
				</th>
				<th>
					Conversions
				</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach($totals as $oss => $stats) { ?>
				<tr>
					<td>
						<?= ucfirst($oss) ?>
					</td>
					<td class="icon">
						<a href="/<?= XXX\Common\Request::getWebsiteId(); ?>/explorer/overview/?fk[os.full]=<?= urlencode($oss) ?>&fk[goal]=<?= $goal['_id'] ?>"><i
									class="ci-recorded-sessions-dark"></i></a>
					</td>
					<td>
						<?= \XXX\Gui\Base\Template\Common::round($stats['views']) ?>
					</td>
					<td>
						<?= \XXX\Gui\Base\Template\Common::round($stats['clicks']) ?>
					</td>
					<td>
						<?= \XXX\Gui\Base\Template\Common::round($stats['conversions']) ?>
					</td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>
<?php } else { ?>
	<div class="content">
		<span class="txt-info">
			<i class="fa-info-circle"></i> There is no operating system data for the dates and filters selected.
		</span>
	</div>
<?php } ?>
