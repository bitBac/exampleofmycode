<?php
$series = [];

foreach($referrers as $referrer) {
	$series[] = array($referrer['_id'], \XXX\Gui\Base\Template\Common::round($referrer['conversions']));
} ?>

<script>
	//require(['highcharts/highcharts', 'extensions/highcharts/XXX-theme'], function() {
	(function () {
		var charts = {};
		charts.referrers = {
			renderReferrers: function () {
				new Highcharts.Chart(Highcharts.merge(Highcharts.XXX.default.options, Highcharts.XXX.default.pie, {
					chart: {
						type: 'pie',
						renderTo: 'chart-referrers'
					},
					series: [{
						name: 'Conversions',
						data: <?=json_encode($series)?>
					}]
				}));
			}
		};
		$(function () {
			charts.referrers.renderReferrers();
		});
	})();
</script>

<div class="content nopadding">
	<div class="chart pie" id="chart-referrers"></div>
</div>
<?php if(!empty($referrers)) { ?>
	<div class="content nopadding">
		<table class="data-table">
			<thead>
			<tr>
				<th class="t35">
					Referrer
				</th>
				<th class="icon">
					<i class="fa-users"></i>
				</th>
				<th>
					Views
				</th>
				<th>
					Clicks
				</th>
				<th>
					Conversions
				</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach($referrers as $referrer) { ?>
				<tr>
					<td>
						<?= $referrer['_id'] ?>
					</td>
					<td class="icon">
						<a href="/<?= XXX\Common\Request::getWebsiteId(); ?>/explorer/overview/?fk[goal]=<?= $goal['_id'] ?>&fk[referrer.host]=<?= urlencode($referrer['_id']) ?>"><i
									class="ci-recorded-sessions-dark"></i></a>
					</td>
					<td>
						<?= \XXX\Gui\Base\Template\Common::round($referrer['views']) ?>
					</td>
					<td>
						<?= \XXX\Gui\Base\Template\Common::round($referrer['clicks']) ?>
					</td>
					<td>
						<?= \XXX\Gui\Base\Template\Common::round($referrer['conversions']) ?>
					</td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>
<?php } else { ?>
	<div class="content">
		<span class="txt-info">
			<i class="fa-info-circle"></i> There is no referrer data for the dates and filters selected.
		</span>
	</div>
<?php } ?>
