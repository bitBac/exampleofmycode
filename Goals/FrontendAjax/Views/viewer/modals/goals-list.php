<div class="modal bultin">
	<div class="header">
		<h4>
			Manage Goals
			<?= \XXX\Gui\Base\Template\Common::docsLink($link = 'http://docs.XXX.com/view/goals/overview.php') ?>
		</h4>
	</div>

	<div class="no-data-notice"><h2 class="title">No Goals Exist!</h2><a class="button action"
	                                                                     data-action="add-new-goal">Create a Goal</a>
	</div>


	<div class="content nopadding">
		<table class="goals-list">
			<thead>
			<tr>
				<th>
					Goal name
				</th>
				<th>
					Goal Rules
				</th>
				<th>
					Multi-Step Settings
				</th>
				<th class="f5">

				</th>
				<th class="f5">
					<i class="fa fa-minus"></i>
				</th>
			</tr>
			</thead>
			<tbody class="goals">

			</tbody>
		</table>
	</div>
	<div class="footer">
		<label class="form-group">
			<a data-action="goal-add" class="button action"> <i class="fa fa-plus"></i> Create new Goal</a>
		</label>
	</div>
</div>

<div class="hidden clone">

</div>

