<div class="modal bultin" id="modal-add-goal" data-onclose="goals-list">
	<form name="modal-add-new-goal">
		<div class="header">
			<h4>
				Add Goal
				<?= \XXX\Gui\Base\Template\Common::docsLink($link = 'http://docs.XXX.com/view/goals/creating-a-goal.php') ?>
			</h4>

			<div class="table-text">
				<label class="fill"> <label>Next Step</label>
					<select data-field="goal-step">
						<option value="next-step">Manage targets for new goal</option>
						<option value="new-goal">Add another goal</option>
						<option value="goals-list">Goto main goal list</option>
					</select>
				</label>
			</div>

		</div>
		<div class="content new-goal">
			<p class="noActiveGoals" style="display:none;">
				You have used all of your active goal allowance. If you create a new goal, it will be created but will
				not be
				set to active.
			</p>

			<p class="form-group">
				<label class="fill">
					<label>
						Goal Name
					</label>
					<input type="text" data-field="goal-name" data-action="goal-name" required placeholder="Name"/>
				</label>
				<label class="fill">
					<label>
						Goal Rules
					</label>
					<select data-field="goal-scope">
						<?php if($experiment instanceof \MongoId) { ?>
							<option value="experiment">Valid in this experiment</option>
						<?php } ?>
						<option value="page">Valid only on a specific page</option>
						<option value="site">Valid on any page on website</option>
					</select>
				</label>

				<label class="fill">
					<label>
						Multi-Step Settings
					</label>
					<select data-field="goal-behaviour">
						<option value="all">All steps are required, any order</option>
						<option value="custom">Some steps required, any order</option>
						<option value="funnel">Advanced conversion funnel</option>

					</select>
				</label>
			</p>
			<button class="button action" data-action="new-goal-submit"><i class="fa fa-circle-plus"></i> Add Goal
			</button>
			<button class="button" data-action="goal-submit-cancel"><i class="fa fa-circle-plus"></i> Cancel</button>
		</div>

	</form>
	<div class="no-data-notice limit hidden">
		<h2 class="title">You have reached your plan limit for Goals per Website</h2>
		<p class="message">To create this Goal you will need to upgrade your plan</p>
		<a class="button action" href="/<?= $websiteId ?>/websites/manage/#plan">Change Plan</a>
		<a class="button action close negative" data-action="goal-limit-cancel">Go Back</a>
	</div>

</div>
