<div class="modal bultin">
	<div class="header">
		<h4>
			<span></span>
			Manage
			Targets <?= \XXX\Gui\Base\Template\Common::docsLink($link = 'http://docs.XXX.com/view/goals/targets.php') ?>
		</h4>
	</div>

	<div class="no-data-notice"><h2 class="title">No Targets Created in this Goal!</h2>
		<a class="button action" data-action="target-create">Create a target</a>
		<a data-action="return-editor" class="button"><i class="fa"></i> Return to Editor</a>
	</div>

	<div class="content nopadding">
		<table class="goals-list">
			<thead>
			<tr>
				<th>
					Target name
				</th>
				<th>
					Type
				</th>
				<th class="f10 order">
					Order
				</th>
				<th class="f10 required">
					Required
				</th>

				<th class="f10 update">

				</th>
				<th class="f5">
					<i class="fa fa-minus"></i>
				</th>
			</tr>
			</thead>
			<tbody class="goals">

			</tbody>
		</table>
	</div>
	<div class="footer">

		<label>
			<a data-action="target-new-create" class="button action"><i class="fa fa-plus"></i> Create new Target</a>
			<!--				<a data-action="clone-goal" class="button action"><i class="fa fa-clone"></i> Clone Goal</a>-->
			<!--				<a data-action="targets-submit" class="button"><i class="fa"></i> Return to Goals</a>-->
			<a data-action="return-editor" class="button"><i class="fa"></i> Return to Editor</a>
		</label>

	</div>

</div>

