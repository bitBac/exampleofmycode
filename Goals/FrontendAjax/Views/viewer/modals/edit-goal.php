<div class="modal" id="modal-add-goal">
	<div class="header">
		<h4>
			Edit Goal
			<?= \XXX\Gui\Base\Template\Common::docsLink($link = 'http://docs.XXX.com/view/goals/overview.php') ?>
		</h4>
		<a class="close fa-times"></a>
	</div>

	<form name="modal-add-new-goal">
		<div class="content new-goal">
			<p data-field="noActiveGoals" class="noActiveGoals" style="display:none;">
				You have used all of your active goal allowance. If you create a new goal, it will be created but will
				not be
				set to active.
			</p>

			<p class="form-group">
				<label class="fill">
					<label>
						Name
					</label>
					<hidden type="text" data-field="goal" required/>
					<input type="text" data-field="goal-name" required placeholder="Name"/>
				</label>
				<label class="fill">
					<label>
						Goal Rules
					</label>
					<select data-field="goal-scope">
						<?php if($experiment instanceof \MongoId) { ?>
							<option value="experiment">Valid in this experiment</option>
						<?php } ?>
						<option value="page">Valid only on a specific page</option>
						<option value="site">Valid on any page on website</option>
					</select>
				</label>

				<label class="fill">
					<label>
						Multi-Step Settings
					</label>
					<select data-field="goal-behaviour">
						<option value="all">All steps are required, any order</option>
						<option value="custom">Some steps required, any order</option>
						<option value="funnel">Advanced conversion funnel</option>
					</select>
				</label>
			</p>
			<button class="button action" data-action="edit-goal-submit"><i class="fa-circle-plus"></i> Save Goal
			</button>
		</div>

	</form>

</div>
