<div class="modal" id="modal-edit-goal-target">
	<div class="header">
		<h4>
			Edit Goal Target
			<?= \XXX\Gui\Base\Template\Common::docsLink($link = 'http://docs.XXX.com/view/goals/overview.php') ?>
		</h4>
		<a class="close fa-times"></a>
	</div>
	<form name="modal-edit-goal-target">
		<input type="hidden" value="" data-field="goal-target-id">
		<div class="content existing-goal">
			<p class="form-group">
				<label class="fill">
					<label>
						Title
					</label>
					<input type="text" data-field="goal-target-title" placeholder="Title" required/>
				</label>
				<label class="fill">
					<label>
						Target Type
					</label>
					<select data-field="goal-target-type">
						<option value="click">Click</option>
						<option value="landing">Landing</option>
						<option value="form">Form submission</option>
					</select>
				</label>
				<span class="fill" data-field="goal-target-order-span">
				<label>
                    Order
                </label>
				<input type="number" data-field="goal-target-order" placeholder="1"/>
			</span>

				<label class="fill hidden">
					<label>
						Target Path
					</label>
					<input type="text" data-field="goal-target-path" placeholder="Target Path"/>
				</label>

				<label class="fill hidden">
					<label>
						Target Added Uri
					</label>
					<input type="text" data-field="goal-target-addedUri" placeholder="Uri where target was added"/>
				</label>
			</p>
			<span data-field="goal-target-required-p">
			<label class="fill">
                <label>
                    <input type="checkbox" data-field="goal-target-required" checked/> Required Target
                </label>
            </label>
            <p></p>
		</span>
			<span data-field="goal-target-uri-p" class="hidden">
			<label>
                Target URI (Regex Compatible)
            </label>
			<input type="text" data-field="goal-target-uri" value="" placeholder="Target URI"/>
			<p></p>
		</span>

			<button data-action="edit-goal-target-submit" class="button action"><i class="fa-circle-plus"></i> Save to
				Goal
			</button>
			<a class="button txt" data-action="goal-toggle-advanced"><i class="fa-gear"></i>Advanced settings</a>
			<a class="button txt close negative"><i class="fa-times"></i> Cancel</a>

		</div>
	</form>
</div>
