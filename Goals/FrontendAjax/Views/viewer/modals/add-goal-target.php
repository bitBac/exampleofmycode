<div class="modal" id="modal-add-goal">
	<div class="header">
		<h4>
			Add Goal Target
			<?= \XXX\Gui\Base\Template\Common::docsLink($link = 'http://docs.XXX.com/view/goals/creating-a-goal.php') ?>
		</h4>
		<a class="close fa-times"></a>
	</div>
	<form name="modal-add-new-goal-target">
		<div class="content existing-goal">
			<span data-field="goal-submit">
				<div class="panel-notification warning" style="padding: 1rem; margin-bottom: 1rem;">
					<i class="fa-info-circle"></i>
					Form may not be submitted as the form is processing by some side script.
				</div>
			</span>
			<p class="form-group">
				<label class="fill">
					<label>
						Title
					</label>
					<input type="text" data-field="goal-target-title" placeholder="Title" required/>
				</label>
				<label class="fill">
					<label>
						Target Type
					</label>
					<select data-field="goal-target-type">
						<option value="click">Click</option>
						<option value="landing">Landing</option>
						<option value="form">Form submission</option>
					</select>
				</label>
				<span class="fill" data-field="goal-target-order-span">
				<label>
					Order
				</label>
				<input type="number" data-field="goal-target-order" placeholder="1"/>
			</span>

				<label class="fill hidden">
					<label>
						Target Path
					</label>
					<input type="text" data-field="goal-target-path" placeholder="Target Path"/>
				</label>

				<label class="fill hidden">
					<label>
						Target Added Uri
					</label>
					<input type="text" data-field="goal-target-addedUri" placeholder="Uri where target was added"/>
				</label>
			</p>
			<span data-field="goal-target-required-p">
			<label class="fill">
				<label>
					<input type="checkbox" data-field="goal-target-required"/> Required Target
				</label>

			</label>
			<p></p>
		</span>
			<span data-field="goal-target-uri-p">
			<label>
				Target URI (Regex Compatible)
			</label>
			<input type="text" data-field="goal-target-uri" value="" placeholder="Target URI" disabled/>
			<p></p>
		</span>

			<button data-action="new-goal-target-submit" class="button action"><i class="fa-circle-plus"></i> Save to
				Goal
			</button>
			<a class="button txt close negative"><i class="fa-times"></i> Cancel</a>

		</div>
	</form>
</div>
