<div class="modal" id="modal-add-goal">
	<div class="header">
		<h4>
			Add Goal Target
			<?= \XXX\Gui\Base\Template\Common::docsLink($link = 'http://docs.XXX.com/view/goals/overview.php') ?>
		</h4>
		<a class="close fa-times"></a>
	</div>
	<div class="content menu">
		<p class="toggles">
			<label>
				<input type="radio" name="goal-type" value="new-goal"/>
				<strong>
					New Goal
					<em class="description">
						Create a new goal for the target you've selected.
					</em>
				</strong>
			</label>
			<label>
				<input type="radio" name="goal-type" value="existing-goal"/>
				<strong>
					Existing Goal
					<em class="description">
						Add the target you've selected to an existing goal.
					</em>
				</strong>
			</label>
		</p>
	</div>
	<div class="contents">

	</div>
	<div class="header new-goal">
		<h4>New Goal</h4>
	</div>

	<form name="modal-add-new-goal">
		<div class="content new-goal">
			<p data-field="noActiveGoals" class="noActiveGoals" style="display:none;">
				You have used all of your active goal allowance. If you create a new goal, it will be created but will
				not be
				set to active.
			</p>
			<p>
				<a data-field="change-goal-type"><i class="fa-arrow-left"></i> Change Goal Type</a>
			</p>
			<p class="form-group">
				<label class="fill">
					<label>
						Name
					</label>
					<input type="text" data-field="goal-name" required placeholder="Name"/>
				</label>
				<label class="fill">
					<label>
						Scope
					</label>
					<select data-field="goal-scope">
						<?php if($experiment instanceof \MongoId) { ?>
							<option value="experiment">Experiment</option>
						<?php } ?>
						<option value="site">Site Wide</option>
					</select>
				</label>
			</p>
		</div>
		<div class="header new-goal">
			<h4>Target Details</h4>
		</div>
		<div class="content new-goal">
			<p class="form-group">
				<label class="fill">
					<label>
						Title
					</label>
					<input type="text" data-field="new-goal-target-title" required placeholder="Title"/>
				</label>
				<label class="fill">
					<label>
						Target Type
					</label>
					<select data-field="new-goal-target-type">
						<option value="click">Click</option>
						<option value="landing">Landing</option>
					</select>
				</label>
				<label class="fill">
					<label>
						Target Path
					</label>
					<input type="text" data-field="new-goal-target-path" required placeholder="Target Path"/>
				</label>
			</p>
			<p>
				<label>
					Target URI (Regex Compatible)
				</label>
				<input type="text" data-field="new-goal-target-uri" value="" required placeholder="Target URI"/>
			</p>
			<button class="button action" data-action="new-goal-submit"><i class="fa-circle-plus"></i> Add Goal</button>
			<a class="button txt close negative"><i class="fa-times"></i> Cancel</a>
		</div>
	</form>

	<div class="header existing-goal">
		<h4>Existing Goal</h4>
	</div>
	<div class="content existing-goal">
		<p>
			<a data-field="change-goal-type"><i class="fa-arrow-left"></i> Change Goal Type</a>
		</p>
		<p>
			<label>
				Goal
			</label>
			<select data-field="existing-goal" class="select-extended-ajax">

			</select>
		</p>
		<p class="form-group">
			<label class="fill">
				<label>
					Title
				</label>
				<input type="text" data-field="existing-goal-target-title" placeholder="Title" required/>
			</label>
			<label class="fill">
				<label>
					Target Type
				</label>
				<select data-field="existing-goal-target-type">
					<option value="click">Click</option>
					<option value="landing">Landing</option>
				</select>
			</label>
			<label class="fill">
				<label>
					Target Path
				</label>
				<input type="text" data-field="existing-goal-target-path" placeholder="Target Path"/>
			</label>
		</p>
		<p>
			<label>
				Target URI (Regex Compatible)
			</label>
			<input type="text" data-field="existing-goal-target-uri" value="" placeholder="Target URI"/>
		</p>
		<a data-action="target-submit" class="button action"><i class="fa-circle-plus"></i> Save to Goal</a><a
				class="button txt close negative"><i class="fa-times"></i> Cancel</a>
	</div>
</div>
