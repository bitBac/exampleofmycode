<div class="modal">
	<div class="header">
		<h4>
			Edit Goal Target
			<?= \XXX\Gui\Base\Template\Common::docsLink($link = 'http://docs.XXX.com/view/goals/overview.php') ?>
		</h4>
		<a class="close fa-times"></a>
	</div>
	<div class="content">
		<p>
			<label>
				Goal
			</label>
			<input type="hidden" data-purpose="goal-id">
			<select data-field="goal" disabled>

			</select>
		</p>
		<p class="form-group">
			<label class="fill">
				<label>
					Title
				</label>
				<input type="text" data-field="title" placeholder="Title" required/>
			</label>
			<label class="fill">
				<label>
					Target Type
				</label>
				<select data-field="type">
					<option value="click">Click</option>
					<option value="landing">Landing</option>
				</select>
			</label>
			<label class="fill">
				<label>
					Target Path
				</label>
				<input type="text" data-field="path" placeholder="Target Path" required/>
			</label>
		</p>
		<p>
			<label>
				Target URI (Regex Compatible)
			</label>
			<input type="text" data-field="uri" value="" placeholder="Target URI" required/>
		</p>
		<div class="modal-options">
			<a data-action="submit" class="button action"><i class="fa-check"></i> Save Changes</a>
			<a data-action="delete" class="button txt close negative"
			   data-confirm="Are you sure that you want to delete this target?"><i class="fa-times"></i> Delete
				Target</a>
		</div>
	</div>
</div>
