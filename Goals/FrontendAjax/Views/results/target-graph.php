<?php foreach((array) $goal['renders']['graphs']['targets'] as $target) {
	$line = array(
		'name' => $target['name'],
		'data' => $target['graphs']['conversions']
	);

	$series[] = $line;
} ?>

<script>
	//require(['highcharts/highcharts', 'extensions/highcharts/XXX-theme'], function() {
	(function () {
		var charts = {};
		charts.overview = {
			renderTarget: function () {
				new Highcharts.Chart(Highcharts.merge(Highcharts.XXX.default.options, Highcharts.XXX.default.line, {
					chart: {
						type: 'area',
						renderTo: 'chart-target'
					},
					series: <?=json_encode($series)?>
				}));
			}
		};
		$(function () {
			charts.overview.renderTarget();
		});
	})();
</script>
<div class="content nopadding">
	<div class="chart line" id="chart-target"></div>
</div>