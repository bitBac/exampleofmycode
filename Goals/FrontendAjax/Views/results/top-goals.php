<?php
foreach((array) $goals as $goal) {
	/**
	 * Organize variation data so we can graph each one over a course of time.
	 */
	$line = array(
		'name' => $goal['name'],
		'data' => $goal['renders']['graphs']['conversions']['conversions']
	);

	$series[] = $line;
} ?>
<script>
	//require(['highcharts/highcharts', 'extensions/highcharts/XXX-theme'], function() {
	(function () {
		var charts = {};
		charts.overview = {
			renderGoals: function () {
				new Highcharts.Chart(Highcharts.merge(Highcharts.XXX.default.options, Highcharts.XXX.default.line, {
					chart: {
						type: 'area',
						renderTo: 'chart-top-goals'
					},
					series: <?= json_encode($series) ?>
				}));
			}
		};
		$(function () {
			charts.overview.renderGoals();
		});
	})();
</script>
<div class="content nopadding">
	<div class="chart line" id="chart-top-goals"></div>
	<?php if($goalsCount == 0) { ?>
		<div class="content">
			<span class="txt-info">
				<i class="fa-exclamation-circle"></i> You don't have any goals. Why not <a
						href="/<?= XXX\Common\Request::getWebsiteId(); ?>/goals/manage/">create one?</a>
			</span>
		</div>
	<?php } else {
		if(empty($goals)) { ?>
			<div class="content">
			<span class="txt-info">
				<i class="fa-exclamation-circle"></i> You don't have any goals conversions for the dates and filters selected.
			</span>
			</div>
		<?php } else { ?>
			<table class="basic-table">
				<thead>
				<tr>
					<th class="t40">
						Goal Name
					</th>
					<th>
						Type
					</th>
					<th>
						Conversions
					</th>
				</tr>
				</thead>
				<tbody>
				<?php foreach($goals as $goal) { ?>
					<tr>
						<td class="target">
							<a href="/<?= XXX\Common\Request::getWebsiteId(); ?>/goals/manage/<?= $goal['_id'] ?>/">
								<?= $goal['name'] ?>
							</a>
						</td>
						<td>
							<?= ucwords($goal['scope']['type']) ?>
						</td>
						<td>
							<?= \XXX\Gui\Base\Template\Common::round($goal['renders']['conversions']['total']) ?>
						</td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		<?php }
	} ?>
</div>
