<script>
	//require(['highcharts/highcharts', 'extensions/highcharts/XXX-theme'], function() {
	(function () {
		var charts = {};
		charts.overview = {
			renderConversions: function () {
				new Highcharts.Chart(Highcharts.merge(Highcharts.XXX.default.options, Highcharts.XXX.default.line, {
					chart: {
						type: 'area',
						renderTo: 'chart-conversions'
					},
					series: [{
						name: 'Total Conversions',
						data: <?=json_encode($report['graphs']['conversions']['conversions'])?>
					}, {
						name: 'New Visitor Conversions',
						data: <?=json_encode($report['graphs']['conversions']['new_visitors'])?>
					}, {
						name: 'Returning Visitor Conversions',
						data: <?=json_encode($report['graphs']['conversions']['returning_visitors'])?>
					}]
				}));
			}
		};
		$(function () {
			charts.overview.renderConversions();
		});
	})();
</script>
<div class="content nopadding">
	<div class="chart line" id="chart-conversions"></div>
	<div class="statistics">
		<div class="statistic">
			<div class="metric">
				<div class="title">
					Conversions
				</div>
				<strong class="value">
					<?= \XXX\Gui\Base\Template\Common::round($report['conversions']['total']) ?>
				</strong>
			</div>
		</div>
		<div class="statistic">
			<div class="metric">
				<div class="title">
					New Visitor Conversions
				</div>
				<strong class="value">
					<?= \XXX\Gui\Base\Template\Common::round($report['conversions']['sessions']['new']) ?>
				</strong>
			</div>
		</div>
		<div class="statistic">
			<div class="metric">
				<div class="title">
					Repeat Visitor Conversions
				</div>
				<strong class="value">
					<?= \XXX\Gui\Base\Template\Common::round($report['conversions']['sessions']['returning']) ?>
				</strong>
			</div>
		</div>
		<div class="statistic">
			<div class="metric">
				<div class="title">
					Avg. Conversions/Day
				</div>
				<strong class="value">
					<?php if($meta['days'] > 0) { ?>
						<?= \XXX\Gui\Base\Template\Common::round(($report['conversions']['total'] / $meta['days']), 2) ?>
					<?php } else { ?>
						0
					<?php } ?>
				</strong>
			</div>
		</div>
	</div>
</div>
