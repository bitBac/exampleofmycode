<?php if(!empty($goal['targets'])) { ?>
	<div class="content nopadding">
		<table class="data-table">
			<thead>
			<tr>
				<th>
					Target
				</th>
				<th>
					Conversions
				</th>
				<th>
					% New Visitors
				</th>
				<th>
					Conversions/Day
				</th>
				<th>
					% of Conversions
				</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach((array) $goal['targets'] as $target) { ?>
				<tr>
					<td>
						<?= $target['name'] ?>
					</td>
					<td>
						<?= \XXX\Gui\Base\Template\Common::round($target['conversions']) ?>
					</td>
					<td>
						<?php if($target['conversions'] > 0) { ?>
							<?= number_format($target['new_visitors'] / $target['conversions'] * 100, 2) ?>%
						<?php } else { ?>
							0%
						<?php } ?>
					</td>
					<td>
						<?php if($meta['days'] > 0) { ?>
							<?= \XXX\Gui\Base\Template\Common::round($target['conversions'] / $meta['days'], 2) ?>
						<?php } else { ?>
							0
						<?php } ?>
					</td>
					<td>
						<?php
						if($goal['renders']['conversions']['total'] > 0) {
							echo number_format($target['conversions'] / $goal['renders']['conversions']['total'] * 100, 2) . '%';
						} else {
							echo '-';
						} ?>

					</td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>
<?php } else { ?>
	<div class="content">
		<span class="txt-info">
			<i class="fa-info-circle"></i> There are no targets for this goal!
		</span>
	</div>
<?php } ?>
