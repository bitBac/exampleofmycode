<div class="panel">
	<div class="header">
		<h2><?= $goal['name'] ?> - <?= $totals['sessions'] ?> entries, <?= $totals['conversions'] ?> conversions
			(<?= $totals['conversionPercent'] ?>%)</h2>
	</div>
	<div class="content nopadding">
		<div class="content nopadding">
			<div class="chart line" id="funnel-chart"></div>
		</div>
		<script>
			var charts = {};
			charts.overview = {
				renderTarget: function () {
					new Highcharts.Chart(Highcharts.merge(Highcharts.XXX.default.options, Highcharts.XXX.default.line, {
						chart: {
							type: 'tfunnel',
							renderTo: 'funnel-chart'
						},
						series: <?= json_encode($series); ?>
					}));
				}
			};
			charts.overview.renderTarget();
		</script>
	</div>
</div>
</div>
