<?php foreach((array) $goal['renders']['graphs']['targets']['column'] as $_id => $target) {
	$series[] = array(
		'name' => $_id,
		'data' => $target
	);
} ?>
<script>
	(function () {
		var charts = {};
		charts.overview = {
			renderTarget: function () {
				new Highcharts.Chart(Highcharts.merge(Highcharts.XXX.default.options, Highcharts.XXX.default.column, {
					chart: {
						type: 'column',
						renderTo: 'bar-target'
					},
					series: <?=json_encode($series)?>
				}));
			}
		};
		$(function () {
			charts.overview.renderTarget();
		});
	})();
</script>
<div class="content nopadding">
	<div class="chart line" id="bar-target"></div>
</div>
