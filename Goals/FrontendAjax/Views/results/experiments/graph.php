<?php if(!empty($series)) { ?>
	<div class="content nopadding">
		<div class="chart line" id="experiment-graph" style="height: 500px;"></div>
	</div>
	<script>
		var charts = {};
		charts.overview = {
			renderTarget: function () {
				new Highcharts.Chart(Highcharts.merge(Highcharts.XXX.default.options, Highcharts.XXX.default.line, {
					chart: {
						type: 'area',
						renderTo: 'experiment-graph'
					},
					series: <?= json_encode($series) ?>
				}));
			}
		};
		charts.overview.renderTarget();
	</script>
<?php } else { ?>
	<div class="content txt-info">
		<i class="fa-exclamation-circle"></i> There is no experiment conversion data for the dates and filters selected.
	</div>
<?php } ?>