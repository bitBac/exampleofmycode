<?php if(!empty($experiments)) { ?>
	<table class="basic-table">
		<thead>
		<tr>
			<th>
				Experiment/Variation
			</th>
			<th>
				Conversions
			</th>
		</tr>
		</thead>
		<tbody>
		<?php foreach((array) $experiments as $_id => $experiment) { ?>
			<tr>
				<td class="target">
					<a href="/<?= XXX\Common\Request::getWebsiteId(); ?>/experiments/manage/<?= $_id ?>/">
						<strong><?= $experiment['name'] ?></strong>
					</a>
				</td>
				<td>
					<?= number_format($experiment['totals']['conversions']) ?>
				</td>
			</tr>
			<?php foreach($experiment['variations'] as $variation_id => $variation) { ?>
				<tr class="subrow">
					<td>
						<?= $variation['name'] ?>
					</td>
					<td>
						<?= \XXX\Gui\Base\Template\Common::round($variation['totals']['conversions']) ?>
					</td>
				</tr>
			<?php } ?>
		<?php } ?>
		</tbody>
	</table>
<?php } else { ?>
	<div class="content">
        <span class="txt-info">
            <i class="fa-exclamation-circle"></i> Goal does not exists on any experiment now.
        </span>
	</div>
<?php } ?>