<table class="data-table">
	<thead>
	<tr>
		<th class="check">
			<label>
				<input type="checkbox"/>
			</label>
		</th>
		<th data-sort="name" class="t50">
			Goal Name
		</th>
		<th data-sort="scope.type">
			Type
		</th>
		<th data-sort="active">
			Status
		</th>
		<th data-sort="targets">
			Targets
		</th>
		<th data-sort="lifetime.conversions">
			Conversions
		</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($goals as $goal) { ?>
		<tr>
			<td class="check">
				<label>
					<input type="checkbox" name="goals[]" value="<?= $goal['_id'] ?>"/>
				</label>
			</td>
			<td class="target">
				<a href="/<?= XXX\Common\Request::getWebsiteId(); ?>/goals/manage/<?= $goal['_id'] ?>/"
				   style="overflow:hidden; white-space:nowrap; text-overflow:ellipsis;">
					<?= $goal['name'] ?>
				</a>
			</td>
			<td>
				<?= ucfirst($goal['scope']['type']) ?>
			</td>
			<td>
				<?= $goal['active'] ? '<i class="txt-positive fa-check-circle"></i> Active' : '<i class="txt-neutral fa-circle"></i> Inactive' ?>
			</td>
			<td>
				<?= count($goal['targets']) ?>
			</td>
			<td>
				<?= \XXX\Gui\Base\Template\Common::round(!empty($goal['renders']['total']['conversions']) ? $goal['renders']['total']['conversions'] : 0) ?>
			</td>
		</tr>
	<?php } ?>
	</tbody>
</table>