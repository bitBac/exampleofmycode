<?php if(!empty($pages)) { ?>
	<table class="data-table">
		<thead>
		<tr>
			<th data-sort="uri">
				URI
			</th>
			<th data-sort="uri">
				Clicks
			</th>
			<th data-sort="uri">
				Hovers
			</th>
			<th data-sort="uri">
				Conversions
			</th>
			<th data-sort="uri">
				Impressions
			</th>
		</tr>
		</thead>
		<tbody>
		<?php foreach($pages as $page) { ?>
			<tr>
				<td class="target">
					<?php
					$site = XXX\Common\Request::getWebsiteId();
					$_page = \XXX\Modules\Pages\Objects\Page::lookup(\MongoDB\castIds(XXX\Common\Request::getWebsiteId()), $page['_id'])
					                                             ->getId();
					$url = "/{$site}/pages/manage/{$_page}/";
					?>
					<a href="<?= $url ?>"><?= $page['_id'] ?></a>
				</td>
				<td>
					<?= \XXX\Gui\Base\Template\Common::round($page['clicks']) ?>
				</td>
				<td>
					<?= \XXX\Gui\Base\Template\Common::round($page['hovers']) ?>
				</td>
				<td>
					<?= \XXX\Gui\Base\Template\Common::round($page['conversions']) ?>
				</td>
				<td>
					<?= \XXX\Gui\Base\Template\Common::round($page['impressions']) ?>
				</td>
			</tr>
		<?php } ?>
		</tbody>
	</table>
<?php } else { ?>
	<div class="content">
        <span class="txt-info">
            <i class="fa-exclamation-circle"></i> Goal does not exists on any page now.
        </span>
	</div>
<?php } ?>