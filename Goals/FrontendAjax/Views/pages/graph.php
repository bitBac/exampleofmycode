<script>
	//require(['highcharts/highcharts', 'extensions/highcharts/XXX-theme'], function() {
	(function () {
		var charts = {};
		charts.overview = {
			renderConversions: function () {
				new Highcharts.Chart(Highcharts.merge(Highcharts.XXX.default.options, Highcharts.XXX.default.line, {
					chart: {
						type: 'area',
						renderTo: 'pages-conversions'
					},
					series: <?= json_encode($series) ?>
				}));
			}
		};
		$(function () {
			charts.overview.renderConversions();
		});
	})();
</script>
<div class="chart line" id="pages-conversions"></div>