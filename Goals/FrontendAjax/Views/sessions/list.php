<?php if(!empty($meta['filter']['identifier'])) { ?>
	<div class="table-filters">
		Filter: <span
				class="filter"><?= $meta['filter']['identifier'] ?></span><?php if(!empty($meta['filter']['value'])) { ?> by
			<span class="filter"><?= $meta['filter']['value'] ?></span><?php } ?>
		<a class="clear-filter" data-tooltip="Clear Filter"
		   href="/<?= XXX\Common\Request::getWebsiteId(); ?>/explorer/overview/"><i
					class="fa-times-circle"></i></a>
	</div>
<?php } ?>
<?php if(!empty($sessions)) { ?>
	<table class="data-table">
		<thead>
		<tr>
			<th data-sort="created">
				Date
			</th>
			<th data-sort="requests_total">
				Page Views
			</th>
			<th>
				Session Length
			</th>
			<th data-sort="summary.totals.clicks">
				Clicks
			</th>
			<th data-sort="summary.totals.conversions">
				Goal Conversions
			</th>
			<th data-sort="viewed">
				Viewed
			</th>
		</tr>
		</thead>
		<tbody>
		<?php foreach($sessions as $session) { ?>
			<tr>
				<td class="target">
					<a href="/<?= XXX\Common\Request::getWebsiteId(); ?>/explorer/session/<?= $session['_id'] ?>/"><?= \XXX\Gui\Base\Template\Common::localize($session['created'], 'F d, Y (g:i a)') ?></a>
				</td>
				<td>
					<?= $session['requests_total'] ?>
					<?php if($session['incomplete']) { ?>
						&nbsp; <a class="txt-info" href="http://docs.XXX.com/view/recorded-sessions/viewer.php"
						          data-tooltip="This sessions may contain incomplete data. Click to view the documentation."
						          data-tooltip-attributes="wrap"><i class="fa-info-circle"></i></a>
					<?php } ?>
				</td>
				<td>
					<?= gmdate("H:i:s", $session['summary']['totals']['time']) ?>
				</td>
				<td>
					<?= $session['summary']['goals']['totals']['clicks'] ?>
				</td>
				<td>
					<?= $session['summary']['goals']['totals']['conversions'] ?>
				</td>
				<td>
					<?= $session['viewed'] ? '<i class="fa-check txt-positive"></i>' : '<i class="fa-circle-o txt-neutral"></i>' ?>
				</td>
			</tr>
		<?php } ?>
		</tbody>
	</table>
<?php } else { ?>
	<div class="content">
		<span class="txt-info">
			<i class="fa-info-circle"></i> There are no recorded sessions for the dates and filters selected.
		</span>
	</div>
<?php } ?>
