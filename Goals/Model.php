<?php
/**
 * XXX
 * Copyright (C) XXX 2014
 */

namespace XXX\Modules\Goals;

use XXX\Common;
use XXX\Modules\Goals;
use XXX\Modules\Experiments;
use XXX\Modules\Websites;
use Flint\Locale;

class Model extends Common\Modules\AbstractModel {
	/**
	 * @param array $ids
	 * @param array $values
	 *
	 * @return mixed
	 */
	public static function updateGoals(array $ids, array $values) {
		return self::databases()
		           ->mongo()
		           ->collection('goals')
		           ->where('_id', 'in', \MongoDb\castIds($ids))
		           ->update(
			           array(
				           '$set' => $values
			           ),
			           true
		           );
	}

	/**
	 * @param array $projection
	 *
	 * @return Objects\GoalsLookup
	 */
	public static function getGoals(array $projection = array()) {
		return new Goals\Objects\GoalsLookup(self::databases()
		                                         ->mongo()
		                                         ->collection('goals')
		                                         ->projection($projection)
		);
	}

	/**
	 * @return Objects\GoalAggregator
	 */
	public static function getGoalsAggregate() {
		return new Goals\Objects\GoalAggregator(self::databases()
		                                            ->mongo()
		                                            ->collection('goals'));
	}

	/**
	 * @param      $goals
	 * @param      $website_id
	 * @param null $obj
	 */
	public static function processViewerGoals($goals, $website_id, $obj = null) {
		foreach($goals as $_id => $submitted_goal) {
			$existing = \MongoDb\castIdsRef($_id);

			if($existing) {
				$goal = Goals\Objects\Goal::load($_id);

				if(isset($submitted_goal['name'])) {
					$goal->set('name', $submitted_goal['name']);
				}
				if(isset($submitted_goal['behaviour'])) {
					$goal->set('behaviour', $submitted_goal['behaviour']);
				}

				if(isset($submitted_goal['active']) && $submitted_goal['active'] == 'false') {
					$goal->set('active', false);
				}
				if(isset($submitted_goal['deleted']) && $submitted_goal['deleted'] == 'true') {
					$goal->set('active', false);
					$goal->set('deleted', true);
				}
				if(!isset($submitted_goal['scope']) && isset($submitted_goal['type'])) {
					$submitted_goal['scope'] = $submitted_goal['type'];
				}
				$goal->setScope(
					$submitted_goal['scope'],
					(($submitted_goal['scope'] == 'experiment' || $submitted_goal['scope'] == 'page') && $obj !== null) ? $obj->getId() : null
				);

			} else {
				/**
				 * Disallow active goals if limit reached
				 */
				$allowed = Websites\Objects\Website::load($website_id)
				                                   ->allowedActiveGoal();

				$goal = Goals\Objects\Goal::create(
					$submitted_goal['name'],
					self::user()
					    ->getAccount()
					    ->getId(),
					\MongoDb\castIds($website_id),
					Locale\Time::utc(),
					null,
					$allowed
				);

				if(get_class($obj) == 'Experiments\Objects\Experiment') {
					$obj->log('goal.add', 'Created New Goal: ' . $goal->get('name'));
				}

				/**
				 * #TODO Renamed scope to goal type in viewer. Temporary for legacy
				 */
				if(!isset($submitted_goal['scope']) && isset($submitted_goal['type'])) {
					$submitted_goal['scope'] = $submitted_goal['type'];
				}

				$goal->setScope(
					$submitted_goal['scope'],
					(($submitted_goal['scope'] == 'experiment' || $submitted_goal['scope'] == 'page') && $obj !== null) ? $obj->getId() : null
				);

				if(isset($submitted_goal['behaviour'])) {
					$goal->set('behaviour', $submitted_goal['behaviour']);
				}

			}

			$targets = json_decode($submitted_goal['targets'], true);

			/**
			 * Remove stored target stats
			 */
			$new_ids = array();
			foreach($targets as $target) {
				$new_ids[] = $target['_id'];
			}

			foreach($goal->get('targets') as $target) {
				if(!in_array($target['_id'], $new_ids)) {
					$goal->removeTargetData($target['_id']);
					if(!empty($experiment)) {
						$experiment->log('target.remove', 'Removed Goal Target: ' . $target['name']);
					}
				}
			}

			$target_save_fields = array(
				'_id',
				'name',
				'type',
				'path',
				'uri',
				'addedUri',
				'subdomain',
				'active',
				'conversions',
				'required',
				'order'
			);

			foreach($targets as &$target) {
				/**
				 * Check type here
				 */
				if(!isset($target['type'])) {
					$target['type'] = 'click';
				}

				$new_target = !\MongoDb\castIdsRef($target['_id']);

				if($new_target) {
					$target['_id'] = new \MongoId();
					if(!empty($experiment)) {
						$experiment->log('target.add', 'Added Goal Target: ' . $target['name']);
					}
				} else {
					$existingGoal = Goals\Objects\Goal::load($_id)
					                                  ->get('targets', array('_id' => $target['_id']))[0];
					if(!empty($experiment)) {
						$fieldsUpdated = array();
						if($target['type'] != $existingGoal['type']) {
							$fieldsUpdated[] = 'Type';
						}
						if($target['name'] != $existingGoal['name']) {
							$fieldsUpdated[] = 'Name';
						}
						if($target['path'] != $existingGoal['path']) {
							$fieldsUpdated[] = 'Path';
						}
						if(!empty($fieldsUpdated)) {
							//$experiment->log('target.edit', 'Goal Target Edited: ' . $target['name'] . ' (Field(s): ' . implode(', ', $fieldsUpdated) . ')');
							//~don't log fields ticket #524
							$experiment->log('target.edit', 'Goal Target Edited: ' . $target['name']);
						}
					}
				}

				/**
				 * Fields whitelist.
				 */
				foreach($target as $key => $value) {
					if(!in_array($key, $target_save_fields)) {
						unset($target[$key]);
					}
				}
			}

			$goal
				->set('targets', $targets)
				->save();
		}
	}

	/**
	 * @param $website_id
	 *
	 * @return int
	 */
	public static function getTotalGoalConversions($website_id) {
		$conversions = 0;

		$goals = self::getGoals()
		             ->website($website_id)
		             ->find();

		foreach($goals as $goal) {
			if(isset($goal['renders']['total']['conversions'])) {
				$conversions += $goal['renders']['total']['conversions'];
			}
		}

		return $conversions;
	}

	/**
	 * @param Websites\Objects\Website $website
	 */
	public static function rebuildGoalTotals(Websites\Objects\Website $website) {
		$applicable = array();
		$goals = Goals\Model::getGoals(array('_id' => 1, 'active' => 1, 'updated' => 1))
		                    ->active(true)
		                    ->website($website->getId())
		                    ->find();

		foreach($goals as $goal) {
			$applicable[] = Goals\Objects\Goal::load($goal['_id']);
		}

		foreach($applicable as $goal) {
			$report = new Goals\Objects\AggregateReports\GoalReport($goal);

			$report->rebuildTotals();
		}
	}
}

