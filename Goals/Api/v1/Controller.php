<?php
/**
 * XXX
 * Copyright (C) XXX 2014
 */

namespace XXX\Modules\Goals\Api\v1;

use XXX\Common;
use XXX\Modules\Goals;
use XXX\Modules\Pages;

class Controller extends Common\Modules\AbstractApiController {

	public static function getGoals() {

		$results = array();

		if(!empty(self::post('goalId'))) {
			$goal = Goals\Objects\Goal::load(self::post('goalId'));

			return array((string) $goal->get('_id') => $goal->get());
		}

		if(!empty(self::post('page')) && strlen(self::post('page')) == 24) {

			$page = Pages\Objects\Page::load(self::post('page'));

			if(self::post('experiment')) {
				$website_id = $page->get('website._id');
			}

			$goals = Goals\Model::getGoals()
			                    ->active(true)
			                    ->hasExperiments(false)
			                    ->website(self::currentWebsite()
			                                  ->getId());

			foreach($goals->find() as $id => $goalArray) {

				$goal = Goals\Objects\Goal::load($goalArray['_id']);
				$targets = $goal->get('targets');

				/*Show empty goals on pages in any case to be able to edit them and add targets*/
				if(empty($targets)) {
					$results[$id] = $goalArray;
				}
				/**
				 * Iterate through the targets and make sure they're valid for this page
				 */
				foreach($targets as $k => $target) {
					if(!$goal->targetAppliesToUri($target['_id'], $page->get('uri'))) {
						unset($targets[$k]);
					}
				}
				/**
				 * If the goal still has targets, add it
				 */
				if(!empty($targets)) {
					$results[$id] = $goalArray;
				}
			}
		}

		if(self::post('website')) {
			$website_id = self::post('website');
		} else {
			if(empty($website_id)) {
				$website_id = self::currentWebsite()
				                  ->getId();
			}
		}

		$goals = Goals\Model::getGoals()
		                    ->active(true)
		                    ->website($website_id);

		if(!empty(self::post('experiment'))) {

			$goals->applicableToExperiment(self::post('experiment'));
			$results += iterator_to_array($goals->find());
		}
		/*A termporary fix for fetching goals for sessions. */
		if(self::posted('page') && strlen(self::post('page')) !== 24) {
			$results = iterator_to_array($goals->find());
		}

		return $results;
	}

	public static function checkFormOnSubmit() {

		$form_count = \XXX\Modules\Sessions\Model::getSessionsAggregate()
		                                              ->website(self::currentWebsite())
		                                              ->parsed(true)
		                                              ->isBot(false)
		                                              ->checkFormSubmit(self::post('goal_path'))
		                                              ->count();

		return ($form_count);
	}

}
