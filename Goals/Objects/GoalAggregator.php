<?php
/**
 * XXX
 * Copyright (C) XXX 2015
 */

namespace XXX\Modules\Goals\Objects;

use XXX\Common;
use XXX\Modules\Goals;
use XXX\Modules\Websites;

class GoalAggregator {

	use Common\Mongo\AggregatorTrait;

	public function website(Websites\Objects\Website $website) {
		$this->baseMatch(array('website._id' => $website->getId()));

		return $this;
	}

	public function active($status = true) {
		$this->baseMatch(array('active' => $status));

		return $this;
	}

	public function deleted($exists = true) {
		$this->baseMatch(array('deleted' => array('$exists' => $exists)));

		return $this;
	}

	public function behaviour($type) {
		$this->baseMatch(array('behaviour' => $type));

		return $this;
	}

}
