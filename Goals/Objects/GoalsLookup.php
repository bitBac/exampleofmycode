<?php
/*
Cycle
Copyright (C) Petrichor 2013
*/

namespace XXX\Modules\Goals\Objects;

use XXX\Common;
use XXX\Modules\Experiments;
use XXX\Modules\Goals;

class GoalsLookup {

	use Common\Mongo\LookupTrait;

	public function website($website) {
		$this->query->where('website._id', \MongoDb\castIds($website));

		return $this;
	}

	public function siteWide() {
		$this->query->where('scope.type', 'site');

		return $this;
	}

	public function hasTargets($has = true) {
		if(!empty($has)) {
			if($has) {
				$this->query->whereExists('targets.0');
			} else {
				$this->query->whereNotExists('targets.0');
			}
		}

		return $this;
	}

	public function hasExperiments($has = true) {

		if($has) {
			$this->query->whereExists('scope.experiment._id');
		} else {
			$this->query->whereNotExists('scope.experiment._id');
		}

		return $this;
	}

	public function active($active) {
		$this->query->where('active', $active);

		return $this;
	}

	public function subdomain($subdomain) {
		if($subdomain && isset($subdomain['_id']) && !empty($subdomain['_id'])) {
			$this->query->where('subdomain', \MongoDb\castIds($subdomain['_id']));
		}

		return $this;
	}

	public function deleted($has = true) {
		if($has) {
			$this->query->whereExists('deleted');
		} else {
			$this->query->whereNotExists('deleted');
		}

		return $this;
	}

	public function applicableToExperiment($_id, $sitewide = true) {
		$this->query
			->whereNested(function($clause) use ($_id, $sitewide) {
				/**
				 * @var \MongoDb\Query\Query $clause
				 */
				$clause->where('scope.experiment._id', \MongoDb\castIds($_id));
				if($sitewide) {
					$clause->where('scope.type', 'site');
				}
			}, 'or');

		return $this;
	}

	public function experiments(array $_ids) {
		$this->query->where('scope.experiment._id', 'in', $_ids);

		return $this;
	}
}
