<?php
/**
 * XXX
 * Copyright (C) XXX 2014
 */

namespace XXX\Modules\Goals\Objects;

use XXX\Common;
use XXX\Modules\Experiments\Objects\Experiment;
use XXX\Modules\Goals;
use XXX\Modules\Insights;
use XXX\Modules\Pages\Objects\Page;
use Flint\Locale;

class Goal {

	use \XXX\Common\Mongo\PrimaryDbTrait, \XXX\Common\Mongo\ObjectDocumentMapperTrait {
		\XXX\Common\Mongo\ObjectDocumentMapperTrait::save as private saveDocument;
		\XXX\Common\Mongo\ObjectDocumentMapperTrait::remove as private removeDocument;
	}

	static $collection = 'goals';

	public static function create($name, $aid, $website, $start, $end = null, $active = false) {
		Goals\Model::databases()
		           ->mongo()
		           ->collection(self::$collection)
		           ->insert(array(
			           'name'      => $name,
			           'active'    => (bool) $active,
			           'aid'       => \MongoDb\castIds($aid),
			           'website'   => array(
				           '_id' => $website
			           ),
			           'start'     => $start,
			           'end'       => $end,
			           'filters'   => array(),
			           'scope'     => array(
				           'type' => 'site'
			           ),
			           'behaviour' => 'all',
			           'targets'   => array(),
			           'lifetime'  => array(
				           'conversions' => 0
			           ),
			           'created'   => Locale\Time::utc()
		           ), array(), $insertId);

		return new Goal($insertId);
	}

	public static function load($_id, $projection = array()) {
		return new Goal($_id, array(), $projection);
	}

	public function setScope($type, $_id = null) {
		$allowed = array(
			'site',
			'page',
			'experiment'
		);

		if(!in_array($type, $allowed)) {
			throw new \Exception('Invalid goal type passed.');
		}

		$this->set('scope', array(
			'type' => $type
		));

		if(!empty($_id)) {
			$this->set('scope.' . $type . '._id', \MongoDb\castIds($_id));

			try {
				if($type == 'page') {
					$page = Page::load($_id);
					$this->set('scope.page.uri', $page->get('uri'));
				}
			} catch(\Exception $e) {

			}
		}

		return $this;
	}

	public function addFilter($type, $value) {
		$allowed = array(
			'os',
			'browser'
		);

		if(!in_array($type, $allowed)) {
			throw new \Exception('Invalid Goal Filter: ' . $type);
		}

		$this->set('filters.?', $value, array($type));

		return $this;
	}

	public function targetAppliesToUri($target_id, $uri) {
		try {
			$target = $this->get('targets.?', array(array('_id' => \MongoDb\castIds($target_id))));
			if(preg_match('~^' . $target['uri'] . '$~', $uri) === 1) {
				return true;
			}
			//for urls with the trailing slash or without '/' at the end to make sure we are able to find data for both. ticket #565
			$uri = ($uri !== '/' && substr($uri, -1) == '/') ? substr($uri, 0, -1) : $uri . '/';
			if(preg_match('~^' . $target['uri'] . '$~', $uri) === 1) {
				return true;
			}

		} catch(\Exception $e) {
			return false;
		}

		return false;
	}

	public function addLifetimeConversion($target_id) {
		/**
		 * Add to target stats
		 */
		$target = $this->get('targets.?', array(array('_id' => \MongoDb\castIds($target_id))));

		if(!empty($target['conversions'])) {
			$target['conversions']++;
		} else {
			$target['conversions'] = 1;
		}

		$this->modify('targets.?', array(array('_id' => \MongoDb\castIds($target_id))), function($target) {
			if(!empty($target['conversions'])) {
				$target['conversions']++;
			} else {
				$target['conversions'] = 1;
			}

			return $target;
		});

		/**
		 * Add to total goal stats
		 */
		$this->modify('lifetime.conversions', array(), function($conversions) {
			$conversions++;

			return $conversions;
		})
		     ->save();
	}

	public function addTarget($name, $type, $path = null, $uri = null, $active = false) {
		$target = array(
			'_id'    => new \MongoId(),
			'name'   => $name,
			'type'   => $type,
			'active' => $active
		);

		if(!empty($path)) {
			$target['path'] = $path;
		}

		if(!empty($uri)) {
			$target['uri'] = $uri;
		}

		$this->push('targets', $target);

		return $this;
	}

	public function removeTargetData($_id) {
		Goals\Model::databases()
		           ->mongo()
		           ->collection('goal_target_stats')
		           ->where('goal', $this->getId())
		           ->where('target', \MongoDb\castIds($_id))
		           ->remove();

		return $this;
	}

	public function deleteGoalData($target_id = null, $lifetime = true) {
		$query = self::mongo()
		             ->collection('goal_target_stats', true)
		             ->where('goal', $this->getId());

		if(!empty($target_id)) {
			$query->where('target', $target_id);
		}

		if($lifetime) {
			foreach((array) $this->get('targets') as $target) {
				$this->set('targets.?.conversions', 0, array(array('_id' => $target['_id'])));
			}

			$this->set('lifetime.conversions', 0);
		}

		$this->save();

		$query->remove();

	}

	public function remove() {
		$insights = Insights\Model::getInsights(array('_id' => 1))
		                          ->extraId_s($this->getId())
		                          ->find();

		foreach($insights as $insight) {
			Insights\Objects\Insight::load($insight['_id'])
			                        ->remove();
		}

		$this->deleteGoalData();
		$this->removeDocument();
	}

	public function save() {
		$this
			->set('updated', Locale\Time::utc())
			->saveDocument();
	}
}

