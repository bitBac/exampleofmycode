<?php
/**
 * XXX
 * Copyright (C) XXX 2014
 */

namespace XXX\Modules\Goals\Objects\AggregateReports;

use XXX\Modules\Experiments;
use XXX\Modules\Goals;
use XXX\Modules\Sessions;
use XXX\Common;
use XXX\Modules\Websites;
use Flint\Locale;

class GoalReport extends Common\Aggregates\AbstractReport {
	/**
	 * @var Goals\Objects\Goal
	 */
	private $goal;

	/**
	 * @var array
	 */
	private $renders = array();

	/**
	 * GoalReport constructor.
	 *
	 * @param Goals\Objects\Goal $goal
	 */
	public function __construct(Goals\Objects\Goal $goal) {
		$this->goal = $goal;
		$this->website = Websites\Objects\Website::load(null, array(), array('_id' => $this->goal->get('website._id')));
	}

	/**
	 * @return Sessions\Objects\SessionAggregator
	 */
	private function baseSessionsAggregation() {
		return Sessions\Model::getSessionsAggregate()
		                     ->website($this->website)
		                     ->dateTimeRange('created', $this->dateTimeRange)
		                     ->goal($this->goal->getId())
		                     ->filter($this->filterGroup)
		                     ->parsed(true)
		                     ->isBot(false);
	}

	/**
	 * @param $goal_id
	 *
	 * @return $this
	 */
	public function goal($goal_id) {
		$this->goal = $goal_id;

		return $this;
	}

	/**
	 * @return $this
	 */
	public function renderSummaryTotals() {
		$result = $this->baseSessionsAggregation()
		               ->project(array(
			               'summary.goals' => 1,
			               'website'       => 1,
			               'parsed'        => 1,
		               ))
		               ->unwind('$summary.goals')
		               ->match(array('summary.goals._id' => $this->goal->getId()))
		               ->buildGroup()
		               ->fields(
			               new Common\Aggregates\Groups\General\TotalSessions(),
			               new Common\Aggregates\Groups\General\SessionTotals(),
			               new Goals\Objects\AggregateReports\Groups\TotalConversions()
		               )
		               ->build()
		               ->aggregate();

		$result = iterator_to_array($result);

		$this->renders['total']['sessions'] = !empty($result) ? $result[0]['sessions'] : 0;
		$this->renders['total']['views'] = !empty($result) ? $result[0]['views'] : 0;
		$this->renders['total']['impressions'] = !empty($result) ? $result[0]['impressions'] : 0;
		$this->renders['total']['clicks'] = !empty($result) ? $result[0]['clicks'] : 0;
		$this->renders['total']['hovers'] = !empty($result) ? $result[0]['hovers'] : 0;
		$this->renders['total']['conversions'] = !empty($result) ? $result[0]['conversions'] : 0;

		return $this;
	}

	/**
	 * @return $this
	 */
	public function renderConversionSummary() {
		$result = $this->baseSessionsAggregation()
		               ->unwind('$summary.goals')
		               ->match(array(
			               'summary.goals._id'                => $this->goal->getId(),
			               'summary.goals.totals.conversions' => array('$gte' => 1)
		               ))
		               ->buildGroup()
		               ->fields(
			               new Common\Aggregates\Groups\General\NewVisitors(),
			               new Common\Aggregates\Groups\General\ReturningVisitors(),
			               new Common\Aggregates\Groups\General\Bounces(),
			               new Common\Aggregates\Groups\General\TotalSessions(),
			               new Common\Aggregates\Groups\General\TotalTime(),
			               new Goals\Objects\AggregateReports\Groups\TotalConversions()
		               )
		               ->build()
		               ->aggregate();

		$result = iterator_to_array($result);

		$this->renders['conversions']['total'] = !empty($result) ? $result[0]['conversions'] : 0;
		$this->renders['conversions']['sessions']['total'] = !empty($result) ? $result[0]['sessions'] : 0;
		$this->renders['conversions']['sessions']['bounced'] = !empty($result) ? $result[0]['bounces'] : 0;
		$this->renders['conversions']['sessions']['new'] = !empty($result) ? $result[0]['new_visitors'] : 0;
		$this->renders['conversions']['sessions']['returning'] = !empty($result) ? $result[0]['returning_visitors'] : 0;
		$this->renders['conversions']['sessions']['total_time'] = !empty($result) ? $result[0]['total_time'] : 0;

		return $this;
	}

	/**
	 * @return $this
	 */
	public function renderTargetSummaries() {
		$result = $this->baseSessionsAggregation()
		               ->unwind('$summary.goals')
		               ->match(array(
			               'summary.goals._id' => $this->goal->getId()
		               ))
		               ->unwind('$summary.goals.targets')
		               ->match(array(
			               'summary.goals.targets.totals.conversions' => array('$gte' => 1)
		               ))
		               ->buildGroup()
		               ->fields(
			               new Common\Aggregates\Groups\General\NewVisitors(),
			               new Goals\Objects\AggregateReports\Groups\TotalTargetConversions()
		               )
		               ->perCustom('$summary.goals.targets._id')
		               ->build()
		               ->sort(new Common\Objects\Sort('conversions', -1))
		               ->aggregate();

		$this->renders['targets'] = iterator_to_array($result);

		foreach($this->renders['targets'] as &$target) {
			$target['name'] = $this->goal->get('targets.?.name', array(array('_id' => $target['_id'])));
		}

		return $this;

	}

	/**
	 * @return $this
	 */
	public function renderTargetEventsColumn() {
		$result = $this->baseSessionsAggregation()
		               ->unwind('$summary.goals')
		               ->match(array(
			               'summary.goals._id' => $this->goal->getId()
		               ))
		               ->unwind('$summary.goals.targets')
		               ->buildGroup()
		               ->fields(
			               new Goals\Objects\AggregateReports\Groups\TotalTargetConversions()
		               )
		               ->field('hovers', array('$sum' => '$summary.goals.targets.totals.hovers'))
		               ->field('clicks', array('$sum' => '$summary.goals.targets.totals.clicks'))
		               ->perCustom('$summary.goals.targets._id')
		               ->build()
		               ->sort(new Common\Objects\Sort('conversions', -1))
		               ->aggregate();

		$results = iterator_to_array($result);

		foreach($results as &$target) {
			$name = $this->goal->get('targets.?.name', array(array('_id' => \MongoDb\castIds($target['_id']))));
			$target['_id'] = $name;
		}

		$this->renders['graphs']['targets']['column'] = $this->buildColumnChart(array(
			'conversions',
			'hovers',
			'clicks'
		), $results);

		return $this;
	}

	/**
	 * @return $this
	 */
	public function renderTargetConversionGraph() {
		$result = $this->baseSessionsAggregation()
		               ->unwind('$summary.goals')
		               ->match(array(
			               'summary.goals._id' => $this->goal->getId()
		               ))
		               ->unwind('$summary.goals.targets')
		               ->buildGroup()
		               ->fields(
			               new Goals\Objects\AggregateReports\Groups\TotalTargetConversions()
		               )
		               ->perDay()
		               ->addIdField('target', '$summary.goals.targets._id')
		               ->build()
		               ->sort(new Common\Objects\Sort('conversions', -1))
		               ->aggregate();

		$result = iterator_to_array($result);

		$targets = array();

		foreach($result as $target) {
			$targets[(string) $target['_id']['target']][] = $target;
		}

		foreach($targets as $_id => $points) {
			$this->renders['graphs']['targets'][$_id] = array(
				'name'   => $this->goal->get('targets.?.name', array(array('_id' => \MongoDb\castIds($_id)))),
				'graphs' => $this->buildLineGraph(array(
					'conversions'
				), $points)
			);
		}

		return $this;
	}

	/**
	 * @return $this
	 */
	public function renderConversionGraph() {
		$result = $this->baseSessionsAggregation()
		               ->unwind('$summary.goals')
		               ->match(array(
			               'summary.goals._id'                => $this->goal->getId(),
			               'summary.goals.totals.conversions' => array('$gte' => 1)
		               ))
		               ->buildGroup()
		               ->fields(
			               new Common\Aggregates\Groups\General\NewVisitors(),
			               new Common\Aggregates\Groups\General\ReturningVisitors(),
			               new Goals\Objects\AggregateReports\Groups\TotalConversions()
		               )
		               ->perDay()
		               ->build()
		               ->aggregate();

		$result = iterator_to_array($result);

		$this->renders['graphs']['conversions'] = $this->buildLineGraph(array(
			'new_visitors',
			'returning_visitors',
			'conversions'
		), $result);

		return $this;
	}

	/**
	 * @param int $limit
	 */
	public function renderOssChart($limit = 5) {
		$aggregate = $this->baseSessionsAggregation()
		                  ->unwind('$summary.goals')
		                  ->match(array(
			                  'summary.goals._id'                => $this->goal->getId(),
			                  'summary.goals.totals.conversions' => array('$gte' => 1)
		                  ))
		                  ->buildGroup()
		                  ->fields(
			                  new Goals\Objects\AggregateReports\Groups\TotalConversions()
		                  )
		                  ->perCustom('$os.name')
		                  ->build()
		                  ->sort(new Common\Objects\Sort('conversions', -1))
		                  ->limit($limit)
		                  ->aggregate();

		$results = iterator_to_array($aggregate);

		$this->renders['charts']['os'] = $this->buildPieChart('conversions', $results);
	}

	/**
	 * @param int $limit
	 *
	 * @return array
	 */
	public function renderOssTotals($limit = 5) {
		$aggregate = $this->baseSessionsAggregation()
		                  ->unwind('$summary.goals')
		                  ->match(array(
			                  'summary.goals._id'                => $this->goal->getId(),
			                  'summary.goals.totals.conversions' => array('$gte' => 1)
		                  ))
		                  ->buildGroup()
		                  ->fields(
			                  new Common\Aggregates\Groups\General\SessionTotals(),
			                  new Goals\Objects\AggregateReports\Groups\TotalConversions()
		                  )
		                  ->perCustom('$os.name')
		                  ->build()
		                  ->sort(new Common\Objects\Sort('conversions', -1))
		                  ->limit($limit)
		                  ->aggregate();

		$results = array();
		foreach($aggregate as $result) {
			if(empty($result['_id'])) {
				$result['_id'] = 'unknown';
			}
			$results[$result['_id']] = $result;
		}

		return $results;
	}

	/**
	 * @param int $limit
	 */
	public function renderDevicesChart($limit = 5) {
		$aggregate = $this->baseSessionsAggregation()
		                  ->unwind('$summary.goals')
		                  ->match(array(
			                  'summary.goals._id'                => $this->goal->getId(),
			                  'summary.goals.totals.conversions' => array('$gte' => 1)
		                  ))
		                  ->buildGroup()
		                  ->fields(
			                  new Goals\Objects\AggregateReports\Groups\TotalConversions()
		                  )
		                  ->perCustom('$device.type')
		                  ->build()
		                  ->sort(new Common\Objects\Sort('conversions', -1))
		                  ->limit($limit)
		                  ->aggregate();

		$results = array();
		foreach($aggregate as $result) {
			if(empty($result['_id'])) {
				$result['_id'] = 'unknown';
			}
			$results[$result['_id']] = $result;
		}

		$this->renders['charts']['devices'] = $this->buildPieChart('conversions', $results);
	}

	/**
	 * @param int $limit
	 *
	 * @return array
	 */
	public function renderDeviceTotals($limit = 5) {
		$aggregate = $this->baseSessionsAggregation()
		                  ->unwind('$summary.goals')
		                  ->match(array(
			                  'summary.goals._id'                => $this->goal->getId(),
			                  'summary.goals.totals.conversions' => array('$gte' => 1)
		                  ))
		                  ->buildGroup()
		                  ->fields(
			                  new Common\Aggregates\Groups\General\SessionTotals(),
			                  new Goals\Objects\AggregateReports\Groups\TotalConversions()
		                  )
		                  ->perCustom('$device.type')
		                  ->build()
		                  ->sort(new Common\Objects\Sort('conversions', -1))
		                  ->limit($limit)
		                  ->aggregate();

		$results = array();
		foreach($aggregate as $result) {
			if(empty($result['_id'])) {
				$result['_id'] = 'unknown';
			}
			$results[$result['_id']] = $result;
		}

		return $results;
	}

	/**
	 * @param int $limit
	 *
	 * @return $this
	 */
	public function renderBrowserChart($limit = 5) {
		$aggregate = $this->baseSessionsAggregation()
		                  ->unwind('$summary.goals')
		                  ->match(array(
			                  'summary.goals._id'                => $this->goal->getId(),
			                  'summary.goals.totals.conversions' => array('$gte' => 1)
		                  ))
		                  ->buildGroup()
		                  ->fields(
			                  new Goals\Objects\AggregateReports\Groups\TotalConversions()
		                  )
		                  ->perCustom('$browser.name')
		                  ->build()
		                  ->sort(new Common\Objects\Sort('conversions', -1))
		                  ->limit($limit)
		                  ->aggregate();

		$results = iterator_to_array($aggregate);

		$this->renders['charts']['browsers'] = $this->buildPieChart('conversions', $results);

		return $this;
	}

	/**
	 * @param int $limit
	 *
	 * @return array
	 */
	public function renderBrowserTotals($limit = 5) {
		$aggregate = $this->baseSessionsAggregation()
		                  ->unwind('$summary.goals')
		                  ->match(array(
			                  'summary.goals._id'                => $this->goal->getId(),
			                  'summary.goals.totals.conversions' => array('$gte' => 1)
		                  ))
		                  ->buildGroup()
		                  ->fields(
			                  new Common\Aggregates\Groups\General\SessionTotals(),
			                  new Goals\Objects\AggregateReports\Groups\TotalConversions()
		                  )
		                  ->perCustom('$browser.name')
		                  ->build()
		                  ->sort(new Common\Objects\Sort('conversions', -1))
		                  ->limit($limit)
		                  ->aggregate();

		$results = array();
		foreach($aggregate as $result) {
			if(empty($result['_id'])) {
				$result['_id'] = 'unknown';
			}
			$results[$result['_id']] = $result;
		}

		return $results;
	}

	/**
	 * @param int $limit
	 *
	 * @return $this
	 */
	public function renderWorldMapConversions($limit = 10) {
		$aggregate = $this->baseSessionsAggregation()
		                  ->unwind('$summary.goals')
		                  ->match(array(
			                  'summary.goals._id'                => $this->goal->getId(),
			                  'summary.goals.totals.conversions' => array('$gte' => 1)
		                  ))
		                  ->buildGroup()
		                  ->fields(
			                  new Goals\Objects\AggregateReports\Groups\TotalConversions(),
			                  new Common\Aggregates\Groups\General\SessionTotals()
		                  )
		                  ->perCustom('$locale.location.country')
		                  ->build()
		                  ->sort(new Common\Objects\Sort('conversions', -1))
		                  ->aggregate();

		$results = iterator_to_array($aggregate);

		$this->renders['conversions']['countries'] = array_slice($results, 0, $limit);

		/**
		 * Render totals, faster than another query
		 */
		$total = 0;
		foreach($this->renders['conversions']['countries'] as $country) {
			$total += $country['conversions'];
		}

		$this->renders['totals']['conversions'] = $total;

		$this->renders['charts']['worldmap'] = $this->buildWorldMap('conversions', $results);

		return $this;
	}

	/**
	 * @param null|string $type
	 * @param int         $limit
	 *
	 * @return $this
	 */
	public function renderReferrers($type = null, $limit = 10) {
		$_id = 'referrer.host';

		switch($type) {
			case 'search':
				$_id = 'referrer.identifier';
				break;
			case 'social':
				$_id = 'referrer.identifier';
				break;
		}

		$aggregate = $this->baseSessionsAggregation()
		                  ->referrer($type)
		                  ->unwind('$summary.goals')
		                  ->match(array(
			                  $_id                               => array(
				                  '$ne' => ''
			                  ),
			                  $_id                               => array(
				                  '$ne' => null
			                  ),
			                  'summary.goals._id'                => $this->goal->getId(),
			                  'summary.goals.totals.conversions' => array('$gte' => 1)
		                  ))
		                  ->buildGroup()
		                  ->fields(
			                  new Goals\Objects\AggregateReports\Groups\TotalConversions(),
			                  new Common\Aggregates\Groups\General\SessionTotals()
		                  )
		                  ->perCustom('$' . $_id)
		                  ->build()
		                  ->sort(new Common\Objects\Sort('conversions', -1))
		                  ->limit($limit)
		                  ->aggregate();

		$results = iterator_to_array($aggregate);

		$this->renders['conversions']['referrers'] = $results;

		return $this;
	}

	/**
	 * Rendering pages where the goal exists
	 */
	public function renderPagesList() {
		$aggregate = $this->baseSessionsAggregation()
		                  ->unwind('$requests')
		                  ->unwind('$requests.goals')
		                  ->match(array(
			                  'requests.goals._id' => array(
				                  '$eq' => $this->goal->getId()
			                  )
		                  ))
		                  ->buildGroup()
		                  ->field('clicks', array('$sum' => '$requests.goals.totals.clicks'))
		                  ->field('hovers', array('$sum' => '$requests.goals.totals.hovers'))
		                  ->field('conversions', array('$sum' => '$requests.goals.totals.conversions'))
		                  ->field('impressions', array('$sum' => 1))
		                  ->perCustom('$requests.uri')
		                  ->build()
		                  ->sort(new Common\Objects\Sort('conversions', -1))
		                  ->paginate(5)
		                  ->aggregate();

		$this->renders = iterator_to_array($aggregate);
	}

	/**
	 * Rendering goal conversions for pages
	 */
	public function renderConversionPages() {
		$aggregate = $this->baseSessionsAggregation()
		                  ->unwind('$requests')
		                  ->unwind('$requests.goals')
		                  ->match(array(
			                  'requests.goals._id'                => array(
				                  '$eq' => $this->goal->getId()
			                  ),
			                  'requests.goals.totals.conversions' => array(
				                  '$gt' => 0
			                  )
		                  ))
		                  ->buildGroup()
		                  ->field('conversions', array('$sum' => '$requests.goals.totals.conversions'))
		                  ->field('name', array('$first' => '$requests.uri'))
		                  ->perDay()
		                  ->addIdField('uri', '$request.uri')
		                  ->build()
		                  ->sort(new Common\Objects\Sort('conversions', -1))
		                  ->aggregate();

		$pages = array();
		foreach(iterator_to_array($aggregate) as $page) {
			$pages[(string) $page['name']][] = $page;
		}

		$graphs = array();
		foreach($pages as $_id => $page) {
			$graph = $this->buildLineGraph(array(
				'conversions'
			), $page);

			$graphs[] = array(
				"name" => $_id,
				"data" => $graph['conversions']
			);
		}

		$this->renders = $graphs;
	}

	/**
	 * Rendering Goal Funnel graph
	 */
	public function renderFunnelGraph() {
		$targets = array();
		foreach($this->goal->get('targets') as $target) {
			$targets[(string) $target['_id']] = array(
				"name"          => $target['name'],
				"order"         => $target['order'],
				"required"      => $target['required'],
				"impressions"   => 0,
				"conversions"   => 0,
				"entered"       => 0,
				"maxImpression" => 0
			);
		}

		$aggregate = $this->baseSessionsAggregation()
		                  ->unwind('$summary.goals')
		                  ->match(array(
			                  'summary.goals._id' => $this->goal->getId()
		                  ))
		                  ->unwind('$summary.goals.targets')
		                  ->group(array(
			                  '_id'         => '$summary.goals.targets._id',
			                  'conversions' => array(
				                  '$sum' => array(
					                  '$cond' => array(
						                  array('$eq' => array('$summary.goals.targets.totals.conversions', 0)), 0, 1
					                  )
				                  )
			                  ),
			                  'impressions' => array(
				                  '$sum' => array(
					                  '$cond' => array(
						                  array('$eq' => array('$summary.goals.targets.totals.impressions', 0)), 0, 1
					                  )
				                  )
			                  ),
			                  'entered'     => array(
				                  '$sum' => array(
					                  '$cond' => array(
						                  array('$gt' => array('$summary.goals.targets.totals.entered', 0)), 1, 0
					                  )
				                  )
			                  )
		                  ))
		                  ->aggregate();

		$maxImpression = 0;
		$results = iterator_to_array($aggregate);

		for($i = 0; $i < count($results); $i++) {
			if(isset($targets[(string) $results[$i]['_id']])) {
				$target = $results[$i];
				$_id = (string) $target['_id'];

				$targets[$_id]['conversions'] = $target['conversions'];
				$targets[$_id]['impressions'] = $target['impressions'];
				$targets[$_id]['entered'] = $target['entered'];
				$targets[$_id]['leaved'] = $target['impressions'] - $target['conversions'];

				if($maxImpression < $targets[$_id]['impressions']) {
					$maxImpression = $targets[$_id]['impressions'];
				}
			}
		}

		uasort($targets, function($a, $b) {
			if($a['order'] == $b['order']) {
				return 0;
			}

			return ($a['order'] < $b['order']) ? -1 : 1;
		});

		$data = array();
		$countSkipped = function($current, $targets) use (&$countSkipped) {
			$nextTarget = current($targets);
			next($targets);

			$skipped = ($nextTarget['impressions'] - $nextTarget['entered']) - $current['impressions'];
			if($nextTarget && !$nextTarget['required'] && current($targets)) {
				return $skipped + $countSkipped($nextTarget, $targets);
			} else {
				return $skipped;
			}
		};

		$goalConversions = $this->renderSummaryTotals();
		$totals = $goalConversions->export()['renders']['total'];
		$totals['conversionPercent'] = ($totals['sessions'] == 0) ? 0 : round($totals['conversions'] * 100 / $totals['sessions']);

		foreach($targets as $target) {
			$target['maxImpression'] = $maxImpression;
			$target['totalTargets'] = count($targets) - 1;
			$target['totalConversionPercent'] = $totals['conversionPercent'];
			$target['totalConversions'] = $totals['conversions'];

			$skipped = $target['required'] ? 0 : $countSkipped($target, $targets);
			$target['skipped'] = $skipped < 0 ? 0 : $skipped;
			$data[] = array('data' => array($target));
		}
		$this->renders['graph'] = $data;
		$this->renders['totals'] = $totals;
	}

	/**
	 * @return $this
	 */
	public function renderExperimentResults() {
		$aggregate = $this->baseSessionsAggregation()
		                  ->unwind('$summary.experiments')
		                  ->unwind('$summary.experiments.variations')
		                  ->unwind('$summary.experiments.variations.goals')
		                  ->match(array(
			                  'summary.experiments.variations.goals._id'                => $this->goal->getId(),
			                  'summary.experiments.variations.goals.totals.conversions' => array(
				                  '$gte' => 1
			                  )
		                  ))
		                  ->group(array(
			                  '_id'         => array(
				                  'experiment' => '$summary.experiments._id',
				                  'variation'  => '$summary.experiments.variations._id',
			                  ),
			                  'conversions' => array(
				                  '$sum' => '$summary.experiments.variations.goals.totals.conversions'
			                  )
		                  ))
		                  ->sort($this->sort)
		                  ->paginate(5)
		                  ->aggregate();

		$results = iterator_to_array($aggregate);

		$experiments = array();
		foreach($results as &$experiment) {
			$tmp = Experiments\Objects\Experiment::load($experiment['_id']['experiment'], array('name' => 1, 'variations' => 1));

			if(!isset($experiments[(string) $experiment['_id']['experiment']])) {
				$experiments[(string) $experiment['_id']['experiment']] = array(
					'name'       => $tmp->get('name'),
					'totals'     => array(
						'conversions' => 0
					),
					'variations' => array()
				);
			}

			if(!isset($experiments[(string) $experiment['_id']['experiment']]['variations'][(string) $experiment['_id']['variation']])) {
				$experiments[(string) $experiment['_id']['experiment']]['variations'][(string) $experiment['_id']['variation']] = array(
					'name'   => $tmp->getVariation($experiment['_id']['variation'])['name'],
					'totals' => array(
						'conversions' => 0
					)
				);
			}

			$experiments[(string) $experiment['_id']['experiment']]['totals']['conversions'] += $experiment['conversions'];
			$experiments[(string) $experiment['_id']['experiment']]['variations'][(string) $experiment['_id']['variation']]['totals']['conversions'] += $experiment['conversions'];

		}

		$this->renders['experiments'] = $experiments;

		return $this;
	}

	/**
	 * Rendering goals conversions graph
	 */
	public function renderExperimentsGraph() {
		$aggregate = $this->baseSessionsAggregation()
		                  ->unwind('$summary.experiments')
		                  ->unwind('$summary.experiments.variations')
		                  ->unwind('$summary.experiments.variations.goals')
		                  ->match(array(
			                  'summary.experiments.variations.goals._id'                => $this->goal->getId(),
			                  'summary.experiments.variations.goals.totals.conversions' => array(
				                  '$gte' => 1
			                  )
		                  ))
		                  ->buildGroup()
		                  ->field('conversions', array('$sum' => '$summary.experiments.variations.goals.totals.conversions'))
		                  ->perDay()
		                  ->addIdField('experiment', '$summary.experiments._id')
		                  ->addIdField('variation', '$summary.experiments.variations._id')
		                  ->build()
		                  ->sort(new Common\Objects\Sort('conversions', -1))
		                  ->aggregate();

		$experiments = array();
		foreach(iterator_to_array($aggregate) as $experiment) {
			$object = Experiments\Objects\Experiment::load($experiment['_id']['experiment']);
			$var = $object->getVariation($experiment['_id']['variation']);
			$expName = $object->get('name');

			$experiments[$expName . " / " . $var['name']][] = $experiment;
		}

		$graphs = array();
		foreach($experiments as $_id => $experiment) {
			$graph = $this->buildLineGraph(array(
				'conversions'
			), $experiment);

			$graphs[] = array(
				"name" => $_id,
				"data" => $graph['conversions']
			);
		}

		$this->renders = $graphs;
	}

	/**
	 * This function will perform renders on this goal from the start of the goal and save to the goal object.
	 */
	public function rebuildTotals() {
		$this->dateRange(new Common\Objects\DateTimeRange(null, Locale\Time::utc()));

		$this->renderSummaryTotals();

		$this->goal->set('renders', $this->renders)
		           ->save();
	}

	/**
	 * Returns the current state of the report
	 *
	 * @return array
	 */
	public function export() {
		$export = $this->goal->get();
		$export['renders'] = $this->renders;

		return $export;
	}
}
