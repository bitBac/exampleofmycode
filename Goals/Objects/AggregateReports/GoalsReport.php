<?php
/**
 * XXX
 * Copyright (C) XXX 2015
 */

namespace XXX\Modules\Goals\Objects\AggregateReports;

use XXX\Modules\Goals;
use XXX\Modules\Sessions;
use XXX\Common;
use XXX\Modules\Websites;
use Flint\Locale;

class GoalsReport extends Common\Aggregates\AbstractReport {

	/**
	 * Returns an array of the top experiments by session for a date range
	 *
	 * @return array
	 * @throws \Exception
	 */
	public function generateIds($limit = null) {
		$activeGoals = array();

		foreach(Goals\Model::getGoals(array('_id' => true))
		                   ->active(true)
		                   ->find() as $goal) {
			$activeGoals[] = $goal['_id'];
		}

		$aggregate = Sessions\Model::getSessionsAggregate()
		                           ->website($this->website)
		                           ->dateTimeRange('created', $this->dateTimeRange)
		                           ->parsed(true)
		                           ->goals($activeGoals)
		                           ->unwind('$summary.goals')
		                           ->group(array(
			                           '_id'            => array(
				                           'website' => '$website._id',
				                           'goal'    => '$summary.goals._id',
			                           ),
			                           $this->sort->key => array(
				                           '$sum' => '$summary.goals.totals.' . $this->sort->key
			                           )
		                           ))
		                           ->sort(new Common\Objects\Sort($this->sort->key, $this->sort->order));

		$aggregate
			->limit($limit);

		$goals = array();

		foreach($aggregate->aggregate() as $result) {

			$goals[] = $result['_id']['goal'];
		}

		return $goals;
	}

	public function getOverview() {
		return Goals\Model::getGoalsAggregate()
		                  ->website($this->website)
		                  ->deleted(false)
		                  ->search(array('name'))
		                  ->aggregate();
	}

	public function getOverviewTotal() {
		$aggregate = Goals\Model::getGoalsAggregate()
		                        ->website($this->website)
		                        ->deleted(false)
		                        ->group(array(
			                        '_id'   => null,
			                        'total' => array('$sum' => 1)
		                        ))
		                        ->search(array('name'))
		                        ->aggregate();

		return iterator_to_array($aggregate);
	}

	public function getOverviewTotalCount() {
		$aggregate = Goals\Model::getGoalsAggregate()
		                        ->website($this->website)
		                        ->deleted(false)
		                        ->group(array(
			                        '_id'   => null,
			                        'total' => array('$sum' => 1)
		                        ))
		                        ->search(array('name'))
		                        ->aggregate();

		return count($aggregate);
	}

	public function generateTotals() {
		$aggregate = Sessions\Model::getSessionsAggregate()
		                           ->website($this->website)
		                           ->dateTimeRange('created', $this->dateTimeRange)
		                           ->parsed(true)
		                           ->unwind('$summary.goals')
		                           ->group(array(
			                           '_id'         => null,
			                           'sessions'    => array('$sum' => 1),
			                           'views'       => array('$sum' => '$summary.goals.totals.views'),
			                           'impressions' => array('$sum' => '$summary.goals.totals.impressions'),
			                           'clicks'      => array('$sum' => '$summary.goals.totals.clicks'),
			                           'hovers'      => array('$sum' => '$summary.goals.totals.hovers'),
			                           'conversions' => array('$sum' => '$summary.goals.totals.conversions')
		                           ))
		                           ->sort(new Common\Objects\Sort($this->sort->key, $this->sort->order))
		                           ->aggregate();

		$result = iterator_to_array($aggregate);

		return array(
			'sessions'    => !empty($result) ? $result[0]['sessions'] : 0,
			'views'       => !empty($result) ? $result[0]['views'] : 0,
			'impressions' => !empty($result) ? $result[0]['impressions'] : 0,
			'clicks'      => !empty($result) ? $result[0]['clicks'] : 0,
			'hovers'      => !empty($result) ? $result[0]['hovers'] : 0,
			'conversions' => !empty($result) ? $result[0]['conversions'] : 0
		);
	}

	public function renderConversionTotals() {
		$result = Sessions\Model::getSessionsAggregate()
		                        ->website($this->website)
		                        ->dateTimeRange('created', $this->dateTimeRange)
		                        ->parsed(true)
		                        ->match(array(
			                        'summary.totals.conversions' => array('$gte' => 1)
		                        ))
		                        ->group(array(
			                        '_id'                => null,
			                        'new_visitors'       => array(
				                        '$sum' => array(
					                        '$cond' => array(
						                        'if'   => array(
							                        '$eq' => array(
								                        '$visitor.new',
								                        true
							                        )
						                        ),
						                        'then' => 1,
						                        'else' => 0
					                        )
				                        )
			                        ),
			                        'returning_visitors' => array(
				                        '$sum' => array(
					                        '$cond' => array(
						                        'if'   => array(
							                        '$eq' => array(
								                        '$visitor.new',
								                        false
							                        )
						                        ),
						                        'then' => 1,
						                        'else' => 0
					                        )
				                        )
			                        ),
			                        'conversions'        => array(
				                        '$sum' => '$summary.totals.conversions'
			                        ),
			                        'sessions'           => array(
				                        '$sum' => 1
			                        )
		                        ))
		                        ->aggregate();

		$result = iterator_to_array($result);

		$results = array();

		$results['conversions']['total'] = !empty($result) ? $result[0]['conversions'] : 0;
		$results['conversions']['sessions']['total'] = !empty($result) ? $result[0]['sessions'] : 0;
		$results['conversions']['sessions']['new'] = !empty($result) ? $result[0]['new_visitors'] : 0;
		$results['conversions']['sessions']['returning'] = !empty($result) ? $result[0]['returning_visitors'] : 0;

		return $results;
	}

	public function renderConversionsGraph() {
		$aggregate = Sessions\Model::getSessionsAggregate()
		                           ->website($this->website)
		                           ->dateTimeRange('created', $this->dateTimeRange)
		                           ->parsed(true)
		                           ->match(array(
			                           'summary.totals.conversions' => array('$gte' => 1)
		                           ))
		                           ->group(array(
			                           '_id'                => array(
				                           'year'  => array(
					                           '$year' => '$created'
				                           ),
				                           'month' => array(
					                           '$month' => '$created'
				                           ),
				                           'day'   => array(
					                           '$dayOfMonth' => '$created'
				                           )
			                           ),
			                           'time'               => array(
				                           '$first' => '$created'
			                           ),
			                           'new_visitors'       => array(
				                           '$sum' => array(
					                           '$cond' => array(
						                           'if'   => array(
							                           '$eq' => array(
								                           '$visitor.new',
								                           true
							                           )
						                           ),
						                           'then' => 1,
						                           'else' => 0
					                           )
				                           )
			                           ),
			                           'returning_visitors' => array(
				                           '$sum' => array(
					                           '$cond' => array(
						                           'if'   => array(
							                           '$eq' => array(
								                           '$visitor.new',
								                           false
							                           )
						                           ),
						                           'then' => 1,
						                           'else' => 0
					                           )
				                           )
			                           ),
			                           'conversions'        => array(
				                           '$sum' => '$summary.totals.conversions'
			                           ),
			                           'sessions'           => array(
				                           '$sum' => 1
			                           )
		                           ))
		                           ->aggregate();

		$results = iterator_to_array($aggregate);

		return $this->buildLineGraph(array('conversions'), $results);
	}

	public function renderGoalsTypes() {
		$aggregate = Goals\Model::getGoalsAggregate()
		                        ->website($this->website)
		                        ->active()
		                        ->group(array(
			                        "_id" => '$scope.type',
			                        'sum' => array(
				                        '$sum' => 1
			                        )
		                        ))
		                        ->aggregate();

		$results = iterator_to_array($aggregate);

		return $this->buildPieChart("sum", $results);
	}

	public function renderConvertedGoals() {
		$converted = Goals\Model::getGoalsAggregate()
		                        ->website($this->website)
		                        ->active()
		                        ->group(array(
			                        "_id" => array(
				                        '$cond' => array(
					                        array(
						                        '$gt' => array(
							                        '$renders.total.conversions', 0
						                        )
					                        ),
					                        "converted",
					                        "not converted"
				                        )
			                        ),
			                        "sum" => array(
				                        '$sum' => 1
			                        )
		                        ))
		                        ->aggregate();

		$results = iterator_to_array($converted);

		return $this->buildPieChart("sum", $results);
	}
}
