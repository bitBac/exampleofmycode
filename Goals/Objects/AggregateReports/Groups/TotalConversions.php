<?php
/**
 * XXX
 * Copyright (C) XXX 2015
 */

namespace XXX\Modules\Goals\Objects\AggregateReports\Groups;

use XXX\Common;

class TotalConversions extends Common\Aggregates\Groups\AbstractField {

	public $group = array(
		'conversions' => array(
			'$sum' => array(
				'$cond' => array(
					array('$gt' => array('$summary.goals.totals.conversions', 0)), 1, 0
				)
			)
		)
	);
}
