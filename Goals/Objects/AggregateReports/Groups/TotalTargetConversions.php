<?php
/**
 * XXX
 * Copyright (C) XXX 2015
 */

namespace XXX\Modules\Goals\Objects\AggregateReports\Groups;

use XXX\Common;

class TotalTargetConversions extends Common\Aggregates\Groups\AbstractField {

	public $group = array(
		'conversions' => array(
			'$sum' => array(
				'$cond' => array(
					array('$gt' => array('$summary.goals.targets.totals.conversions', 0)), 1, 0
				)
			)
		)
	);
}
