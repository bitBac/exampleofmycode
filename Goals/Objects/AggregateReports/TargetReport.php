<?php
/**
 * XXX
 * Copyright (C) XXX 2014
 */

namespace XXX\Modules\Goals\Objects\AggregateReports;

use XXX\Modules\Goals;
use XXX\Modules\Sessions;
use XXX\Common;
use XXX\Modules\Websites;
use Flint\Locale;

class TargetReport extends Common\Aggregates\AbstractReport {
	/**
	 * @var Goals\Objects\Goal
	 */
	private $goal;

	private $target = null;

	/**
	 * @var array
	 */
	private $renders = array();

	public function __construct(Goals\Objects\Goal $goal, $target = null) {
		$this->goal = $goal;
		$this->target = \MongoDb\castIds($target);
		$this->website = Websites\Objects\Website::load(null, array(), array('_id' => $this->goal->get('website._id')));
	}

	public function renderSummaryTotals() {
		$result = Sessions\Model::getSessionsAggregate()
		                        ->project(array(
			                        'summary.goals' => 1,
			                        'website'       => 1,
			                        'parsed'        => 1,
		                        ))
		                        ->website($this->website)
		                        ->dateTimeRange('created', $this->dateTimeRange)
		                        ->goal($this->goal->getId())
		                        ->parsed(true)
		                        ->unwind('$summary.goals')
		                        ->match(array('summary.goals._id' => $this->goal->getId()))
		                        ->unwind('$summary.goals.targets')
		                        ->match(array('summary.goals.targets._id' => $this->target))
		                        ->group(array(
			                        '_id'         => array(
				                        'goal'   => '$summary.goals._id',
				                        'target' => '$summary.goals.targets._id'
			                        ),
			                        'sessions'    => array('$sum' => 1),
			                        'impressions' => array('$sum' => '$summary.goals.totals.impressions'),
			                        'clicks'      => array('$sum' => '$summary.goals.totals.clicks'),
			                        'hovers'      => array('$sum' => '$summary.goals.totals.hovers'),
			                        'conversions' => array('$sum' => '$summary.goals.totals.conversions')
		                        ))
		                        ->aggregate();

		$result = iterator_to_array($result);

		$this->renders['total']['sessions'] = !empty($result) ? $result[0]['sessions'] : 0;
		$this->renders['total']['impressions'] = !empty($result) ? $result[0]['impressions'] : 0;
		$this->renders['total']['clicks'] = !empty($result) ? $result[0]['clicks'] : 0;
		$this->renders['total']['hovers'] = !empty($result) ? $result[0]['hovers'] : 0;
		$this->renders['total']['conversions'] = !empty($result) ? $result[0]['conversions'] : 0;

		return $this;
	}

	public function renderConversionSummary() {
		$result = Sessions\Model::getSessionsAggregate()
		                        ->website($this->website)
		                        ->dateTimeRange('created', $this->dateTimeRange)
		                        ->goal($this->goal->getId())
		                        ->parsed(true)
		                        ->unwind('$summary.goals')
		                        ->match(array('summary.goals._id' => $this->goal->getId()))
		                        ->match(array('summary.goals.totals.conversions' => array('$gte' => 1)))
		                        ->unwind('$summary.goals.targets')
		                        ->match(array('summary.goals.targets._id' => $this->target))
		                        ->match(array('summary.goals.targets.totals.conversions' => array('$gte' => 1)))
		                        ->buildGroup()
		                        ->fields(
			                        new Common\Aggregates\Groups\General\NewVisitors(),
			                        new Common\Aggregates\Groups\General\ReturningVisitors(),
			                        new Common\Aggregates\Groups\General\Bounces(),
			                        new Common\Aggregates\Groups\General\TotalSessions(),
			                        new Common\Aggregates\Groups\General\TotalTime(),
			                        new Goals\Objects\AggregateReports\Groups\TotalTargetConversions()
		                        )
		                        ->build()
		                        ->aggregate();

		$result = iterator_to_array($result);

		$this->renders['conversions']['total'] = !empty($result) ? $result[0]['conversions'] : 0;
		$this->renders['conversions']['sessions']['total'] = !empty($result) ? $result[0]['sessions'] : 0;
		$this->renders['conversions']['sessions']['bounced'] = !empty($result) ? $result[0]['bounces'] : 0;
		$this->renders['conversions']['sessions']['new'] = !empty($result) ? $result[0]['new_visitors'] : 0;
		$this->renders['conversions']['sessions']['returning'] = !empty($result) ? $result[0]['returning_visitors'] : 0;
		$this->renders['conversions']['sessions']['total_time'] = !empty($result) ? $result[0]['total_time'] : 0;

		return $this;
	}

	/**
	 * This function will perform renders on this experiment from the start of the experiment and save to the
	 * experiment object.
	 */
	public function rebuildTotals() {
		$this->dateRange(new Common\Objects\DateTimeRange(null, Locale\Time::utc()));

		$this->renderSummaryTotals();

		$this->goal->set('renders', $this->renders)
		           ->save();
	}

	/**
	 * Returns the current state of the report
	 *
	 * @return array
	 */
	public function export() {
		$export = $this->goal->get('targets.?', array(array('_id' => $this->target)));
		$export['renders'] = $this->renders;

		return $export;
	}
}
